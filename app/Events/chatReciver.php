<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class chatReciver implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $mensaje;
    public $usuario;
    public $hora;
    public $sala;

    public function __construct($mensaje, $usuario, $hora, $sala)
    {
        $this->mensaje = $mensaje;
        $this->usuario = $usuario;
        $this->hora = $hora;
        $this->sala = $sala;
    }

    public function broadcastOn()
    {
        return ["chat-channel"];
    }

    public function broadcastAs()
    {
        return "chat-event";
    }
}
