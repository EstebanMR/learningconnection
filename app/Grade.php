<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    protected $table = 'grade';
    protected $hidden = ['id'];
    public $timestamps = false;
    protected $fillable = ['id', 'grade_name','grade_number', 'active', 'state'];
}
