<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $usernames = DB::table('users')->where('type', 2)->orwhere('type', 1)->where('state', 1)->get();
        $users = [];
        $temp=DB::table('users')
        ->select(DB::raw('count(*) as cant'))
        ->where('state', 1)
        ->where('type', 2)
        ->orwhere('type', 3)
        ->groupBy('type')
        ->get();
        foreach ($temp as $user){
            array_push($users, $user->cant);
        }
        $users = implode(",", $users);
        $materials = [];
        $teachers = [];
        $temp1=DB::table('material')
        ->select(DB::raw('user_id, count(*) as cant'))
        ->where('state', 1)
        ->where('active', 1)
        ->groupBy('user_id')
        ->take(5)
        ->get();
        foreach ($temp1 as $material){
            array_push($materials, $material->cant);
            foreach($usernames as $names){
                if ($names->id === $material->user_id) {
                    array_push($teachers, $names->username);
                }
            }
        }
        $color1 = "'".implode("','", $this->randColor(count($teachers)))."'";
        $teachers = "'".implode("','", $teachers)."'";
        $materials = implode(",", $materials);
        $rooms = [];
        $term = [];
        $temp2 = DB::table('study_room')
        ->join('grade', 'study_room.grade_id', '=', 'grade.id')
        ->join('subject', 'study_room.subject_id', '=', 'subject.id')
        ->select(DB::raw('count(study_room.name) as cant, grade.grade_name as grade, subject.subject as subject'))
        ->where('study_room.state', true)
        ->groupBy('grade.grade_name', 'subject.subject')
        ->take(5)
        ->get();
        foreach ($temp2 as $room){
            array_push($rooms, $room->cant);
            array_push($term, $room->grade."-".$room->subject);
        }
        $rooms = implode(",", $rooms);
        $term = "'".implode("','", $term)."'";
        return view('admin')->with('users', $users)->with('materials', $materials)->with('teachers', $teachers)->with('color1', $color1)->with('term', $term)->with('rooms', $rooms);
    }

    public function randColor($cant)
    {
        $color = ['#E62C28', '#0D5DA7', '#F1B809'];
        $n = $cant-count($color);
        if ($n>0) {
            for ($i=0; $i < $n; $i++) { 
                $co = '#'.substr(str_shuffle('ABCDEF0123456789'), 0, 6);
                array_push($color, $co);
            }
        } else {
            while($n<=0){
                array_pop($color);
                $n = count($color);
            }
        }
        return $color;
    }
}
