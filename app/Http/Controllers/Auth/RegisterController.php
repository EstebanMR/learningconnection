<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'lastname' => ['required', 'string', 'max:255'],
            'age' => ['required', 'integer'],
            'institute' => ['string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'type' => ['required', 'integer'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        //dd($data);
        $n = $data['name'];
        $l = "";
        $u = "";
        $temp = explode(" ", $data['lastname']);
        if(!isset($temp[1])) {
            $l = $data['lastname'];
            $u = strtolower("@".$n[0].$l);
        } else {
            $l = explode(" ", $data['lastname']);
            $u = strtolower("@".$n[0].$l[0].$l[1][0]);
        }
        $age = intval($data['age']);
        $type = intval($data['type']);
        return User::create([
            'name' => $data['name'],
            'lastname' => $data['lastname'],
            'username' => $u,
            'age' => $age,
            'institute' => $data['institute'],
            'type' => $type,
            'email' => $data['email'],
            'password' => Hash::make($data['password'])
        ]);
    }


}
