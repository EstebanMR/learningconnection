<?php

namespace App\Http\Controllers;

use App\content;
use App\Subject;
use App\Grade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subjects= Subject::where('state', 1)->reorder('active')->get();
        $grades= Grade::where('state', 1)->reorder('active')->get();
        $contents= Content::where('state', 1)->reorder('active')->get();
        return view('content')->with('contents', $contents)->with('grades', $grades)->with('subjects', $subjects);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ($_POST['id'] === "") {
            $content = new Content(['subject_id' => $_POST['subject'], 'grade_id' => $_POST['grade'], 'content' => $_POST['name'], 'active' => true]);
            $content->save();
            return redirect('content');
        } else {
            $content = new Content(['id' => intval($_POST['id']), 'subject_id' => $_POST['subject'], 'grade_id' => $_POST['grade'], 'content' => $_POST['name'], 'active' => true]);
            Content::Where("id", $content->id)->update(['subject_id' => $content->subject_id, 'grade_id' => $content->grade_id, 'content' => $content->content, 'active' => true]);
            return redirect('content');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\content  $content
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, content $content)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\content  $content
     * @return \Illuminate\Http\Response
     */
    public function destroy(request $request)
    {
        $content = Content::findOrFail($request->id1);
        $content->state = false;
        $content->save();
        return redirect('content');
    }
}
