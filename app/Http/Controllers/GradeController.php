<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Grade;
use Illuminate\Support\Facades\Auth;

class GradeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $grades= Grade::where('state', 1)->reorder('active')->get();
        return view('grade')->with('grades' , $grades);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ($_POST['id'] === "") {
            $grade = new Grade(['grade_name' => $_POST['name'], 'grade_number' => $_POST['grade'], 'active' => true]);
            $grade->save();
            if (!isset($grade->id)) {
                return redirect('grade')->with('error', 'El nivel no ha sido creado!'); 
            }
            return redirect('grade')->with('success', 'Nivel creado con éxito!');
        } else {
            $grade = new Grade(['id' => intval($_POST['id']), 'grade_name' => $_POST['name'], 'grade_number' => $_POST['grade'], 'active' => true]);
            $grade = Grade::Where("id", $grade->id)->update(['grade_name' => $_POST['name'], 'grade_number' => $_POST['grade'], 'active' => true]);
            if (is_null($grade)) {
                return redirect('grade')->with('error', 'El nivel no ha sido editado!'); 
            }
            return redirect('grade')->with('success', 'Material editado con éxito!');
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\content  $content
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, content $content)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(request $request)
    {
        $grade = Grade::findOrFail($request->id1);
        $grade->state = false;
        $grade->save();
        return redirect('grade')->with('error', 'Nivel eliminado con éxito!');
    }
}
