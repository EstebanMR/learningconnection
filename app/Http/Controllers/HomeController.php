<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Providers\RouteServiceProvider;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->state === 0) {
            Auth::logout();
        }
        switch ($user->type) {
            case 1:
                return redirect('/admin');
                break;
            case 2:
                return redirect('/teacherp');
                break;
            case 3:
                return redirect('/student');
                break;
            default:
                # code...
                break;
        }
        //
    }
}
