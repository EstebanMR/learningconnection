<?php

namespace App\Http\Controllers;

use App\material;
use App\content;
use App\Subject;
use App\Grade;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class MaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subjects= Subject::where('state', 1)->reorder('active')->get();
        $grades= Grade::where('state', 1)->reorder('active')->get();
        $contents= Content::where('state', 1)->reorder('active')->get();
        $materials= Material::where('state', 1)->reorder('active')->get();
        $users= DB::table('users')->get();
        return view('material')->with('materials' , $materials)->with('users', $users)->with('contents', $contents)->with('grades', $grades)->with('subjects', $subjects);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $link;
        if ($_POST['type'] === "PDF" || $_POST['type'] === "Documento de texto" || $_POST['type'] === "Imagen" || $_POST['type'] === "video") {
            $link = $request->file('doc')->store('public/material');
        } else {
            $link = $_POST['link'];
        }
        if ($_POST['id'] === "") {
            $material = new Material(['user_id' => Auth::user()->id, 'link'=>$link, 'name' => $_POST['name'], 'grade_id' => $_POST['grade'], 'subject_id' => $_POST['subject'], 'content_id' => $_POST['content'], 'points' => 0, 'type' => $_POST['type'], 'process' => 'processed', 'active' => true]);
            $material->save();
            if (!isset($material->id)) {
                return redirect('material')->with('error', 'El material no ha sido creado!');
            }
            return redirect('material')->with('success', 'Material creado con éxito!');
        } else {
            $materialf = Material::findOrFail($_POST['id']);
            $material = new Material(['id' => intval($_POST['id']), 'user_id' => $materialf->user_id, 'link'=>$_POST['link'], 'name' => $_POST['name'], 'grade_id' => $_POST['grade'], 'subject_id' => $_POST['subject'], 'content_id' => $_POST['content'], 'type' => $_POST['type'], 'process' => 'processed', 'active' => true]);
            $material = Material::Where("id", $material->id)->update([ 'link'=> $link, 'name' => $material->name, 'grade_id' => $material->grade_id, 'subject_id' => $material->subject_id, 'content_id' => $material->content_id, 'type' => $material->type]);
            if (is_null($material)) {
                return redirect('material')->with('error', 'El material no ha sido editado!'); 
            }
            return redirect('material')->with('success', 'Material editado con éxito!');
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @return \Illuminate\Http\Response
     */
    public function accept(request $request)
    {
        $teacher = Material::findOrFail($request->idt);
        Material::Where("id", $teacher->id)->update([ 'process' => 'processed','active' => true, 'state' => true ]);
        return redirect('material')->with('success', 'Material aceptado con éxito!'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\material  $material
     * @return \Illuminate\Http\Response
     */
    public function destroy(request $request)
    {
        $material = Material::findOrFail($request->id1);
        $material->state = false;
        $material->active = false;
        $material->save();
        return redirect('material')->with('success', 'Material eliminado con éxito!');
    }
}
