<?php

namespace App\Http\Controllers;

Use App\Score;
use App\material;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class MaterialUserController extends Controller
{
    public $grade;
    public $subject;
    public $content;
    public $type;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($grade, $subject, $content, $type)
    {
        $this->grade = $grade;
        $this->subject = $subject;
        $this->content = $content;
        $this->type = $type;
        return view('materialUser')->with("grade", $this->grade)->with("subject", $this->subject)->with("content", $this->content)->with("type", $this->type); 
    }

    /**
     * Update the specified resource in storage.
     *
     */
    public function update()
    {
        $score1 = new Score(['id_material' => $_POST['id1'], 'id_usuario' => Auth::user()->id, 'points' => $_POST['points']]);
        $score1->save();
        $scores = Score::where('id_material', $_POST['id1'])->get();
        $n = $scores->count();
        $suma = 0;
        $promedio = 0;
        foreach ($scores as $score){
            $suma += $score->points;
        }
        $promedio = $suma / $n;
        $material = Material::find($_POST['id1']);
        $material->points = $promedio;
        $material->save();
        return back();
    }
}
