<?php

namespace App\Http\Controllers;

use App\User;
use App\Grade;
use App\Student;
use App\Teacher;
use App\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role;
        $user = User::where('id', '=', Auth::user()->id)->where('state', 1)->first();
        $subjects = Subject::where('state', 1)->reorder('active')->get();
        $grades = Grade::where('state', 1)->reorder('active')->get();
        switch ($user->type) {
            case 2:
                $role = Teacher::where('user_id', Auth::user()->id)->first();
                break;
            case 3:
                $role = Student::Where('user_id', Auth::user()->id)->first();
                break;
            default:
                abort(403);
                break;
        }
        //dd($user);
        //dd($role);
        return view('profile')->with('user', $user)->with('role', $role)->with('grades', $grades)->with('subjects', $subjects);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     */
    public function edit()
    {
        $user = User::where('id', '=', Auth::user()->id)->where('state', 1)->first();
        $user->name = $_POST['name'];
        $user->lastname = $_POST['lastname'];
        $user->email = $_POST['email'];
        $user->age = $_POST['age'];
        $user->institute = $_POST['institute'];
        if ($_POST['password'] != "userpassword") {
            $user->password = Hash::make($_POST['password']);
        }
        $user->save();
        if ($user->type === 3) {
            $student = Student::Where('user_id', Auth::user()->id)->first();
            $student->grade = $_POST['grade'];
            $student->save();
        } elseif ($user->type === 2) {
            $teacher = Student::Where('user_id', Auth::user()->id)->first();
            $teacher->subject_focus = $_POST['subject'];
            $teacher->save();
        }
        return back()->with('success', 'Usuario actualizado!');
    }

    public function destroy(request $request)
    {
        $user = User::findOrFail(Auth::user()->id);
        $user->state = false;
        $user->save();
        if ($user->type === 3) {
            $student = Student::Where('user_id', Auth::user()->id)->first();
            $student->state = false;
            $student->save();
        } elseif ($user->type === 2) {
            $teacher = Student::Where('user_id', Auth::user()->id)->first();
            $teacher->state = false;
            $teacher->save();
        }
        Auth::logout();
        return redirect('login');
    }
}
