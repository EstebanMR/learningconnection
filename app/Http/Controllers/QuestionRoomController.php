<?php

namespace App\Http\Controllers;

use App\Grade;
use App\Teacher;
use App\Student;
use App\Subject;
use App\content;
use App\material;
Use Carbon\Carbon;
use App\mesages_qroom;
use App\Question_room;
use App\material_qroom;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class QuestionRoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->type === 3) {
            $users = DB::table('users')->where('type', '>', 1)->where('state', 1)->get();
            $rooms = Question_room::where('state', 1)->Where('id_student', Auth::user()->id)->get();
            $subjects = Subject::where('state', 1)->reorder('active')->get();
            $grades = Grade::where('state', 1)->reorder('active')->get();
            $contents = Content::where('state', 1)->reorder('active')->get();
            //dd($rooms);
            return view('userQuestions')->with('contents', $contents)->with('grades', $grades)->with('subjects', $subjects)->with('users', $users)->with('rooms', $rooms);
        } elseif (Auth::user()->type === 2) {
            $teacher = Teacher::where('user_id', Auth::user()->id)->first();
            $users = DB::table('users')->where('type', '>', 1)->where('state', 1)->get();
            if ($teacher->institute_focus === "Colegio") {
                $rooms = Question_room::where('state', true)->Where('grade_id', '<', 9)->where('subject_id', $teacher->subject_focus)->WHERE('id_teacher', null)->get();
            } elseif ($teacher->institute_focus === "Escuela") {
                $rooms = Question_room::where('state', 1)->Where('grade_id', '>', 8)->get()>WHERE('id_teacher', null);
            }
            $myrooms = Question_room::where('state', 1)->Where('id_teacher', Auth::user()->id)->get();
            $subjects = Subject::where('state', 1)->reorder('active')->get();
            $contents = Content::where('state', 1)->reorder('active')->get();
            return view('teacherQuestions')->with('contents', $contents)->with('subjects', $subjects)->with('users', $users)->with('rooms', $rooms)->with('myrooms', $myrooms);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexRoom($id)
    {
        $usertype = Auth::user()->type;
        $room = Question_room::where('id', $id)->first();
        $materialR = Material_qroom::where('id_room', $room->id)->where('state', 1)->get();
        $materials= Material::where('state', 1)->where('active', 1)->where('subject_id', $room->subject_id)->where('grade_id', $room->grade_id)->where('content_id', $room->content_id)->get();
        $users = DB::table('users')->where('type', '>', 1)->where('state', 1)->get();
        return view('qroom')->with('room' , $room)->with('users', $users)->with('materialR', $materialR)->with('materials', $materials)->with('usertype', $usertype);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $student = Student::Where('user_id', Auth::user()->id)->first();
        $temp = new Question_room();
        $temp->id_student = Auth::user()->id;
        $temp->subject_id = $_POST['subject'];
        $temp->content_id = $_POST['content'];
        $temp->grade_id = $student->grade;
        $temp->content = $_POST['question'];
        $temp->save(); 
        if (!isset($temp->id)) {
            return redirect('questionRoom')->with('error', 'La consulta no ha sido creada!');
        }

        $temp1 = new Mesages_qroom();
        $temp1->id_room = $temp->id;
        $temp1->id_user = $temp->id_student;
        $temp1->message = $temp->content;
        $temp1->date_sms = Carbon::now()->format('Y/m/d'); 
        $temp1->time_sms = date('H:i', time());
        $temp1->save();
        return redirect('questionRoom')->with('success', 'Consulta creada con éxito!');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(request $request)
    {
        $room = Question_room::where('id', $request->id1)->update(['state'=>false]);
        return redirect('questionRoom')->with('success', 'Sala eliminada con éxito!');
    }

    public function acceptqroom($id)
    {
        $room = Question_room::where('id', $id)->update(['id_teacher'=>Auth::user()->id]);
        if (is_null($room)) {
            return redirect('questionRoom')->with('error', 'No se pudo aceptar la sala! intente más tarde');
        }else{
            $this->indexRoom($id);
        }
    }


    public function addMaterial()
    {
        $idMaterial = $_POST['idm'];
        $idRoom = $_POST['idr'];
        $mr = new Material_qroom(['id_room'=>$idRoom, 'id_material'=>$idMaterial, 'state'=>1 ]);
        $mr->save();
        return back();
    }

    public function destroyM(request $request)
    {
        $material = Material_qroom::findOrFail($request->id);
        $material->state = false;
        $material->save();
        return back();
    }
}
