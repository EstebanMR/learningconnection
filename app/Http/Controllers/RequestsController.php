<?php

namespace App\Http\Controllers;

use App\material;
use App\content;
use App\Subject;
use App\Grade;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class RequestsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subjects= Subject::where('state', 1)->reorder('active')->get();
        $grades= Grade::where('state', 1)->reorder('active')->get();
        $contents= Content::where('state', 1)->reorder('active')->get();
        $materials= Material::where('state', 1)->reorder('active')->get();
        $users= DB::table('users')->get();
        return view('Requests')->with('materials' , $materials)->with('users', $users)->with('contents', $contents)->with('grades', $grades)->with('subjects', $subjects);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $link;
        if ($_POST['type'] === "PDF" || $_POST['type'] === "Documento de texto" || $_POST['type'] === "Imagen" || $_POST['type'] === "video") {
            $link = $request->file('doc')->store('public/material');
        } else {
            $link = $_POST['link'];
        }
        $material = new Material(['user_id' => Auth::user()->id, 'link'=>$link, 'name' => $_POST['name'], 'grade_id' => $_POST['grade'], 'subject_id' => $_POST['subject'], 'content_id' => $_POST['content'], 'points' => 0, 'type' => $_POST['type'], 'process' => 'not processed', 'active' => false]);
        $material->save();
        return redirect('teacherrequests')->with('success', 'Solicitud enviada!');
    }
}
