<?php

namespace App\Http\Controllers;

use App\Grade;
use App\Student;
Use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()) {
            $temp =  Student::Where('user_id', Auth::user()->id)->first();
            if (!empty($temp)) {
                $frase;
                $frases = [['frase' => 'El único modo de hacer un gran trabajo es amor lo que haces.', 'autor' => 'Steve Jobs'], 
                ['frase' => 'La pasión es energía. Siente el poder que viene de centrarte en lo que te emociona.', 'autor' => 'Oprah Winfrey'], 
                ['frase' => 'Continúa a pesar de que todos esperen que abandones. No dejes que se oxide el hierro que hay en ti.', 'autor' => 'Teresa de Calcuta'],
                ['frase' => 'El genio se hace con un 1% de talento, y un 99% de trabajo.', 'autor' => 'Albert Einstein'],
                ['frase' => 'Hay una fuerza motriz más poderosa que el vapor, la electricidad y la energía atómica: la voluntad.', 'autor' => 'Albert Einstein'],
                ['frase' => 'Confía en ti mismo sin importar los que los demás piensen.', 'autor' => 'Arnold Schwarzenegger'], 
                ['frase' => 'No te amargues con tu propio fracaso ni se lo cargues a otro. Acéptate ahora o seguirás justificándote como un niño. Recuerda que cualquier momento es bueno para comenzar y que ninguno es tan terrible para claudicar.', 'autor' => 'Pablo Neruda'],
                ['frase' => 'Nunca se ha logrado nada sin entusiasmo.', 'autor' => 'Emerson'], 
                ['frase' => 'Nuestra gloria más grande no consiste en no haberse caído nunca, sino en haberse levantado después de cada caída.', 'autor' => 'Confucio'], 
                ['frase' => 'Pregúntate si lo que estás haciendo hoy te acerca al lugar en el que quieres estar mañana.', 'autor' => 'Walt Disney'], 
                ['frase' => 'Importa mucho más lo que tú piensas de ti mismo que lo que los otros piensen de ti. ', 'autor' => 'Séneca'], 
                ['frase' => 'Si exagerásemos nuestras alegrías como lo hacemos con nuestras penas, nuestros problemas perderían toda su importancia.', 'autor' => 'Anónimo'], 
                ['frase' => 'Ten el coraje para hacer lo que te dice tu corazón y tu intuición.', 'autor' => 'Steve Jobs'], 
                ['frase' => 'Siempre se puede, cuando se quiere.', 'autor' => 'José Luis Sampedro'],
                ['frase' => 'La mejor forma de predecir el futuro es crearlo.', 'autor' => 'Abraham Lincoln'],
                ['frase' => 'Cree en ti mismo y en lo que eres. Se consciente de que hay algo en tu interior que es más grande que cualquier obstáculo.', 'autor' => 'Anónimo'],
                ];
                $fecha = Carbon::now()->format('d'); 
                switch ($fecha) {
                    case '1':
                        $frase = $frases[0];
                        break;
                    case '2':
                        $frase = $frases[1];
                        break;
                    case '3':
                        $frase = $frases[2];
                        break;
                    case '4':
                        $frase = $frases[3];
                        break;
                    case '5':
                        $frase = $frases[4];
                        break;
                    case '6':
                        $frase = $frases[5];
                        break;
                    case '7':
                        $frase = $frases[6];
                        break;
                    case '8':
                        $frase = $frases[7];
                        break;
                    case '9':
                        $frase = $frases[8];
                        break;
                    case '10':
                        $frase = $frases[9];
                        break;
                    case '11':
                        $frase = $frases[10];
                        break;
                    case '12':
                        $frase = $frases[11];
                        break;
                    case '13':
                        $frase = $frases[12];
                        break;
                    case '14':
                        $frase = $frases[13];
                        break;
                    case '15':
                        $frase = $frases[14];
                        break;
                    case '16':
                        $frase = $frases[15];
                        break;
                    case '17':
                        $frase = $frases[0];
                        break;
                    case '18':
                        $frase = $frases[1];
                        break;
                    case '19':
                        $frase = $frases[2];
                        break;
                    case '20':
                        $frase = $frases[3];
                        break;
                    case '21':
                        $frase = $frases[4];
                        break;
                    case '22':
                        $frase = $frases[5];
                        break;
                    case '23':
                        $frase = $frases[6];
                        break;
                    case '24':
                        $frase = $frases[7];
                        break;
                    case '25':
                        $frase = $frases[8];
                        break;
                    case '26':
                        $frase = $frases[9];
                        break;
                    case '27':
                        $frase = $frases[10];
                        break;
                    case '28':
                        $frase = $frases[11];
                        break;
                    case '29':
                        $frase = $frases[12];
                        break;
                    case '30':
                        $frase = $frases[13];
                        break;
                    case '32':
                        $frase = $frases[14];
                        break;
                }
                //dd($frase);
                if (session()->has('tostudy')) {
                    $list = session('tostudy');
                    $list = json_decode($list);
                    return view('student')->with('frase', $frase)->with('list', $list);
                }
                return view('student')->with('frase', $frase);
            } else {
                $grades= Grade::where('state', 1)->reorder('active')->get();
                return view('studentData')->with('grades', $grades);
            }
        } else {
            return redirect('login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $student = new Student(['user_id' => Auth::user()->id, 'grade' => $_POST['grade']]);
        $student->save();
        return redirect('student')->with('success', 'Usuario actualizado!');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createTask()
    {
        $task = $_POST['task'];
        $tasks = [];
        if (session()->has('tostudy')) {
            $list = session('tostudy');
            $tasks = json_decode($list, true);
        }
        array_push($tasks, $task);
        $tasks = json_encode($tasks);
        session(['tostudy' => $tasks]);
        return back();
    }

    public function deleteTask($study)
    {
        if (session()->has('tostudy')) {
            $list = session('tostudy');
            $tasks = json_decode($list, true);
            //dd($tasks);
            $clave = array_search($study, $tasks);
            unset($tasks[$clave]);
            //dd($tasks);
        }
        $tasks = json_encode($tasks);
        session(['tostudy' => $tasks]);
        return back();
    }
}
