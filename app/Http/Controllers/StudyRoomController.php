<?php

namespace App\Http\Controllers;

use App\Grade;
use App\Subject;
use App\content;
use App\Study_room;
use App\Users_room;
use App\material;
use App\material_srooms;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class StudyRoomController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = DB::table('users')->where('type', '=', 3)->where('state', 1)->get();
        $subjects = Subject::where('state', 1)->reorder('active')->get();
        $grades = Grade::where('state', 1)->reorder('active')->get();
        $contents = Content::where('state', 1)->reorder('active')->get();
        $rooms = Study_room::where('state', 1)->get();
        $users_rooms = Users_room::where('state', 1)->get();
        return view('studyRooms')->with('auth' , Auth::user()->id)->with('users' , $users)->with('subjects', $subjects)->with('grades', $grades)->with('contents', $contents)->with('rooms', $rooms)->with('users_rooms', $users_rooms);
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexRoom($id)
    {
        $room = Study_room::where('id', $id)->first();
        $materialR = Material_srooms::where('id_room', $room->id)->where('state', 1)->get();
        $materials= Material::where('state', 1)->where('active', 1)->where('subject_id', $room->subject_id)->where('grade_id', $room->grade_id)->get();
        $users = DB::table('users')->where('type', '=', 3)->where('state', 1)->get();
        $users_room = Users_room::where('state', 1)->where('id_room', $id)->get();
        return view('sroom')->with('users_room' , $users_room)->with('room' , $room)->with('users', $users)->with('materialR', $materialR)->with('materials', $materials);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $users = DB::table('users')->where('type', '=', 3)->where('state', 1)->get();
        $invitados = $_POST['users'];
        $invitados = str_replace(" ","", $invitados);
        if (str_contains($invitados, ',')) {
            $invitados = explode(",", $invitados);
            $tmp=[];
            $i=0;
            foreach ($invitados as $invitado) {
                foreach ($users as $user){
                    if ($user->username === $invitado) {
                        $invitados[$i] = $user->id;
                        $i++;
                    }
                }
            }
            $invitados= array_unique($invitados);
        }else{
            foreach ($users as $user){
                if ($user->username == $invitados) {
                    $invitados = $user->id;
                }
            }
        }
        $creador = auth::user()->id;
        $temp=DB::table('student')->where('user_id', '=', $creador)->where('state', 1)->get('grade');
        $grade=$temp[0]->grade;
        if ($_POST['id'] != "") {
            $room=Study_room::Where("id", $_POST['id'])->update([ 'name'=> $_POST['name'], 'date_room' => $_POST['date'], 'time_room' => $_POST['time'], 'subject_id' => $_POST['subject'], 'content_id' => $_POST['cont1'], 'content1_id' => $_POST['cont2'], 'content2_id' => $_POST['cont3'] ]);
            if (is_null($room)) {
                return redirect('studyRoom')->with('error', 'La sala no se ha editada!');
            }
            $room = Study_room::Where("id", $_POST['id'])->first();
            $guests = Users_room::select('id_user')->where('id_room', $room->id)->where('state', 1)->get();
            $temp = [];
            foreach ($guests as $guest){
                array_push($temp, $guest->id_user);
            }
            $guests = $temp;
            if (is_array($invitados) && count($guests)<10) {
                $i = 1;
                foreach ($invitados as $invitado) {
                    if ($invitado !== '') {
                        $user= new Users_room(['position'=>$i, 'id_room'=>$_POST['id'], 'id_user'=>$invitado, 'max_users'=> 10, 'state=>true']);
                        if (!in_array($invitado, $guests)) {
                            $user->save();
                            echo($invitado);
                        }
                        $i++;
                    }
                }
            } else {
                if ($invitado !== '') {
                    $user= new Users_room(['position'=>1, 'id_room'=>$_POST['id'], 'id_user'=>$invitados, 'max_users'=> 10, 'state=>true']);
                    $user->save();
                }
            }
            return redirect('studyRoom')->with('success', 'Material acctualizado con éxito!');
        }else{
            $room = new Study_room(['name'=> $_POST['name'], 'creator' => $creador, 'date_room' => $_POST['date'], 'time_room' => $_POST['time'], 'subject_id' => $_POST['subject'], 'content_id' => $_POST['cont1'], 'content1_id' => $_POST['cont2'], 'content2_id' => $_POST['cont3'], 'grade_id' => $grade]);
            $room->save();
            if (!isset($room->id)) {
                return redirect('studyRoom')->with('error', 'La sala no se ha creado!');
            }
            if (is_array($invitados)) {
                $i =1;
                foreach ($invitados as $invitado) {
                    if ($invitado !== '') {
                        $user= new Users_room(['position'=>$i, 'id_room'=>$room->id, 'id_user'=>$invitado, 'max_users'=> 10, 'state=>true']);
                        $user->save();
                        $i++;
                    }
                }
            } else {
                if ($invitados != '') {
                    $user= new Users_room(['position'=>1, 'id_room'=>$room->id, 'id_user'=>$invitados, 'max_users'=> 10, 'state=>true']);
                    $user->save();
                }
            }
            $sql = 'SELECT * FROM material WHERE grade_id = '.$room->grade_id.' AND state = 1 AND active = 1 AND subject_id ='.$room->subject_id.' AND (content_id = '.$room->content_id.' OR content_id = '.$room->content1_id.' OR content_id = '.$room->content2_id.')';
            $materials=DB::select($sql);
            if (count($materials)>0) {
                if (count($materials)>3) {
                    $i=count($materials);
                    for ($j=0; $j < 3 ; $j++) { 
                        $in= intval(rand(0, $i));
                        $mr = new Material_Srooms(['id_room'=>$room->id, 'id_material'=>$material[$int]->id, 'state'=>1 ]);
                        $mr->save();
                    }

                }else if(count($materials)<=3 && count($materials)>0){
                    foreach($materials as $material){
                        $mr = new Material_Srooms(['id_room'=>$room->id, 'id_material'=>$material->id, 'state'=>1 ]);
                        $mr->save();
                    }
                }

            }
            return redirect('studyRoom')->with('success', 'Sala creada con éxito!');
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(request $request)
    {
        $room = Study_room::findOrFail($request->id1);
        $room->state = false;
        $us = Users_room::where('id_room', $room->id)->get();
        foreach ($us as $u){
            $u->state = false;
            $u->save();
        }
        $room->save();
        return redirect('studyRoom')->with('success', 'Sala eliminada con éxito!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function exit(request $request)
    {
        $us = Users_room::where('id_room', $request->id1)->get();
        foreach ($us as $u){
            if ($u->id_user === auth::user()->id) {
                $u->state = false;
                $u->save();
            }
        }
        return redirect('studyRoom')->with('success', 'Has salido de la sala con éxito!');
    }

    public function addMaterial()
    {
        $idMaterial = $_POST['idm'];
        $idRoom = $_POST['idr'];
        $mr = new Material_Srooms(['id_room'=>$idRoom, 'id_material'=>$idMaterial, 'state'=>1 ]);
        $mr->save();
        return redirect('sroom/'.$idRoom);
    }

    public function destroyM(request $request)
    {
        $material = Material_Srooms::findOrFail($request->id);
        $material->state = false;
        $material->save();
        return redirect('sroom/'.$material->id_room)->with('success', 'Material eliminado con éxito!');
    }
}
