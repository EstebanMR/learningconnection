<?php

namespace App\Http\Controllers;

use App\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subjects= Subject::where('state', 1)->reorder('active')->get();
        return view('subject')->with('subjects' , $subjects);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ($_POST['id'] === "") {
            $subject = new Subject(['subject' => $_POST['name'], 'active' => true]);
            $subject->save();
            if (!isset($subject->id)) {
                return redirect('subject')->with('error', 'La signatura no ha sido creada!');
            }
            return redirect('subject')->with('success', 'Asignatura creado con éxito!');
        } else {
            $subject = new Subject(['id' => intval($_POST['id']), 'subject' => $_POST['name'], 'active' => true]);
            $subject = Subject::Where("id", $_POST['id'])->update(['subject' => $_POST['name'], 'active' => true]);
            if (is_null($subject)) {
                return redirect('subject')->with('error', 'La signatura no ha sido creada!');
            }
            return redirect('subject')->with('success', 'Asignatura editada con éxito!');
        }

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\content  $content
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, content $content)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Subject  $subject
     * @return \Illuminate\Http\Response
     */
    public function destroy(request $request)
    {
        $subject = Subject::findOrFail($request->id1);
        $subject->state = false;
        $subject->save();
        return redirect('subject')->with('success', 'Asignatura eliminada con éxito!');
    }
}
