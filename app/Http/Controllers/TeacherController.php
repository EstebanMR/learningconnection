<?php

namespace App\Http\Controllers;

use App\Teacher;
use App\Subject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class TeacherController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = DB::table('users')->where('type', '<=', 2)->where('state', 1)->get();
        $teachers = Teacher::where('state', 1)->reorder('active')->get();
        $subjects= Subject::where('state', 1)->reorder('active')->get();
        return view('teacherCRUD')->with('users' , $users)->with('teachers', $teachers)->with('subjects', $subjects);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->hasFile('docs')) {
            $count = count($request->file('docs'));
            if ($count > 0) {
                foreach ($request->file('docs') as $item){
                    $item->store("public/".$_POST['user'], 'local');
                }
            }
        }
        if ($_POST['id'] != "") {
            $teacher = Teacher::findOrFail($_POST['id']);
            $teacher->institute_focus = $_POST['institute'];
            $teacher->cicle_focus = $_POST['cicle'];
            $teacher->subject_focus = $_POST['subject'];
            $teacher = teacher::Where("id", $teacher->id)->update([ 'institute_focus'=> $teacher->institute_focus, 'cicle_focus' => $teacher->cicle_focus, 'subject_focus' => $teacher->subject_focus ]);
            if (is_null($teacher)) {
                return redirect('teacher')->with('error', 'El docente no ha sido editado!');
            }
            return redirect('teacher')->with('success', 'Asignatura editado con éxito!');
        }else{
            $teacher = new Teacher(['user_id' => $_POST['user'], 'institute_focus'=>$_POST['institute'], 'cicle_focus' => $_POST['cicle'], 'subject_focus' => $_POST['subject'], 'active' => true, 'state' => true]);
            $teacher->save();
            if (!isset($teacher->id)) {
                return redirect('teacher')->with('error', 'El docente no ha sido creado!');
            }
            return redirect('teacher')->with('success', 'Docente creado con éxito!');
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @return \Illuminate\Http\Response
     */
    public function accept(request $request)
    {
        $teacher = Teacher::findOrFail($request->idt);
        teacher::Where("id", $teacher->id)->update([ 'active' => true, 'state' => true ]);
        return redirect('teacher')->with('success', 'Docente aceptado con éxito!'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(request $request)
    {
        $teacher = Teacher::findOrFail($request->id1);
        $teacher->state = false;
        $teacher->active = false;
        $teacher->save();
        return redirect('teacher')->with('success', 'Docente eliminado con éxito!');
    }
}
