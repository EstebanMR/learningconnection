<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Teacher;
use App\Subject;

class TeacherProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()) {
            $temp =  Teacher::Where('user_id', Auth::user()->id)->where('state', 1)->where('active', 1)->first();
            if (!empty($temp)) {
                return redirect('questionRoom');
            } else {
                $subjects = Subject::where('state', 1)->reorder('active')->get();
                return view('teacherData')->with('subjects', $subjects);
            }
        } else {
            return redirect('login');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->hasFile('docs')) {
            $count = count($request->file('docs'));
            if ($count > 0) {
                foreach ($request->file('docs') as $item){
                    $item->store("public/".Auth::user()->id, 'local');
                }
            }
        }
        $teacher = new Teacher(['user_id' => Auth::user()->id, 'institute_focus'=>$_POST['institute'], 'cicle_focus' => $_POST['cicle'], 'subject_focus' => $_POST['subject'], 'active' => false]);
        $teacher->save();
        return redirect('/teacherp')->with('success', 'Solicitud de docente enviada con éxito!, Espere su autorización');
        
    }
}
