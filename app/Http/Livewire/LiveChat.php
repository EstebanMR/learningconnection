<?php

namespace App\Http\Livewire;

use App\User;
Use Carbon\Carbon;
use Livewire\Component;
use App\mesages_srooms;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class LiveChat extends Component
{
    public $mensajes;
    public $sala;
    public $usuario;

    protected $listeners = ["mensajeEnviado"];

    public function mount($room)
    {
        $this->mensajes = [];
        $this->sala = $room;
        $this->usuario = Auth::user()->username ?? "";
    }

    public function mensajeEnviado($sms)
    {
        if ($sms["sala"]===$this->sala) {
            $this->updatesms($sms);
        }
    }

    public function updatesms($sms)
    {
        if ($this->usuario != "") {
            $mensajes = Mesages_Srooms::where('id_room', '=', $sms["sala"])->get();
            $users = DB::table('users')->where('type', '=', 3)->where('state', 1)->get();
            $this->mensajes = [];
            foreach ($mensajes as $mensaje){
                foreach ($users as $user){
                    if ($mensaje->id_user === $user->id ) {
                        $item=[
                            "mensaje" => $mensaje->message,
                            "sala" => $mensaje->id_room,
                            "usuario" => $user->username,
                            "hora" => Carbon::parse($mensaje->time_sms)->format('h:i a')
                        ];
                        array_push($this->mensajes, $item);
                    }
                }
            }
        } 
        
    }

    public function render()
    {
        $temp = ["sala"=>$this->sala];
        $this->mensajeEnviado( $temp);
        return view('livewire.live-chat');
    }
}
