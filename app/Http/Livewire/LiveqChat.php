<?php

namespace App\Http\Livewire;

use App\User;
Use Carbon\Carbon;
use App\mesages_qroom;
use Livewire\Component;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class LiveqChat extends Component
{
    public $mensajes;
    public $sala;
    public $usuario;

    protected $listeners = ["mensajeEnviado"];

    public function mount($room)
    {
        $this->mensajes = [];
        $this->sala = $room;
        $this->usuario = auth::user()->username ?? "";
        $this->selectmsms($room);
    }

    public function render()
    {
        return view('livewire.liveq-chat');
    }

    public function mensajeEnviado($sms)
    {
        if ($sms["sala"]===$this->sala) {
            $this->updatesms($sms);
        }
    }

    public function updatesms($sms)
    {
        if ($this->usuario != "") {
            $mensajes = Mesages_qroom::where('id_room', '=', $sms["sala"])->get();
            $users = DB::table('users')->where('type', '>', 1)->where('state', 1)->get();
            $this->mensajes = [];
            foreach ($mensajes as $mensaje){
                foreach ($users as $user){
                    if ($mensaje->id_user === $user->id ) {
                        $item=[
                            "mensaje" => $mensaje->message,
                            "sala" => $mensaje->id_room,
                            "usuario" => $user->username,
                            "hora" => Carbon::parse($mensaje->time_sms)->format('h:i a')
                        ];
                        array_push($this->mensajes, $item);
                    }
                }
            }
        } 
        
    }

    public function selectmsms($room)
    {
        if ($this->usuario != "") {
            $mensajes = Mesages_qroom::where('id_room', '=', $room)->get();
            //dd($mensajes);
            $users = DB::table('users')->where('type', '>', 1)->where('state', 1)->get();
            $this->mensajes = [];
            foreach ($mensajes as $mensaje){
                foreach ($users as $user){
                    if ($mensaje->id_user === $user->id ) {
                        $item=[
                            "mensaje" => $mensaje->message,
                            "sala" => $mensaje->id_room,
                            "usuario" => $user->username,
                            "hora" => Carbon::parse($mensaje->time_sms)->format('h:i a')
                        ];
                        array_push($this->mensajes, $item);
                    }
                }
            }
        } 
    }
}
