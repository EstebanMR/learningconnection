<?php

namespace App\Http\Livewire;

use App\Grade;
use App\Subject;
use App\content;
use App\material;
use Livewire\Component;
use Illuminate\Support\Facades\DB;

class MaterialUser extends Component
{
    public $materials;
    public $grades;
    public $subjects;
    public $contents;
    public $grade;
    public $subject;
    public $content;
    public $type;
    public $types;

    public function mount($grade, $subject, $content, $type)
    {
        $this->materials = Material::where('state', 1)->where('active', '1')->get();
        $this->grades = Grade::where('state', 1)->where('active', '1')->get();
        $this->subjects = Subject::where('state', 1)->where('active', '1')->get();
        $this->contents = content::where('state', 1)->where('active', '1')->get();
        $this->grade = $grade;
        $this->subject = $subject;
        $this->content = $content;
        $this->type = $type;
        $this->types = ["PDF", "Documento de texto", "Imagen", "Video", "Enlace de PDF web", "Enlace de documento web", "Enlace de pagina web", "Enlace de imagen web", "Enlace de video web"];
    }

    public function render()
    {
        $sql= "SELECT * FROM material WHERE state = 1 AND active = 1";
        if ($this->grade != 0 || $this->subject != 0  || $this->content != 0  || $this->type != 0 ) {
            if ($this->grade != 0) {
                $sql = $sql." AND grade_id = ".$this->grade;

            } 
            if ($this->subject != 0) {
                $sql = $sql." AND subject_id = ".$this->subject;

            } 
            if ($this->content != 0) {
                $sql = $sql." AND content_id = ".$this->content;

            } 
            foreach ($this->types as $type){
                if ($this->type == $type) {
                    $sql = $sql." AND type like '".$this->type."'";
                }
            }
            //dd($sql, $this->subject);
            $this->materials = DB::select($sql);
        }
        return view('livewire.material-user',["materials"=>$this->materials, 'grades'=>$this->grades, 'subjects'=>$this->subjects, 'contents'=>$this->contents]);
    }

    public function filter()
    {
        if ($this->grade != 0 || $this->subject != 0  || $this->content != 0  || $this->type != 'NULL' ) {

            return redirect()->route('materialUser', ['grade' => $this->grade,'subject' =>  $this->subject, 'content' => $this->content, 'type' => $this->type]);
        }
    }

}
