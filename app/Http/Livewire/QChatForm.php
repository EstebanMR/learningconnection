<?php

namespace App\Http\Livewire;

Use Carbon\Carbon;
use App\mesages_qroom;
use Livewire\Component;
use Illuminate\Support\Facades\Auth;

class QChatForm extends Component
{
    public $mensaje;
    public $usuario;
    public $hora;
    public $sala;
    public $fecha;
    public $caracteres;

    public function mount($room)
    {
        $this->mensaje = "";
        $this->usuario = Auth::user()->id;
        $this->hora = "";
        $this->sala = $room;
        $this->fecha = "";    
    }

    public function render()
    {
        return view('livewire.q-chat-form');
    }

    public function enviarMensaje()
    {
        $this->validate([
            "mensaje" => "required|max:550"
        ]);
        
        $this->hora = date('H:i', time());
        $this->fecha = Carbon::now()->format('Y/m/d'); 
        

        $data = new Mesages_qroom([
            "message" => $this->mensaje,
            "id_user" => $this->usuario,
            "id_room" => $this->sala,
            "date_sms" => $this->fecha,
            "time_sms" => $this->hora,
        ]);
        $data->save();
        event(new \App\Events\qchatReciver($this->mensaje,$this->usuario, $this->hora, $this->sala));
        $this->mensaje='';
    }
}
