<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class question_room extends Model
{
    protected $table = 'question_room';
    protected $hidden = ['id'];
    public $timestamps = false;
    protected $fillable = ['id', 'max_users', 'id_student', 'id_teacher', 'subject_id', 'content_id', 'content', 'state'];
}
