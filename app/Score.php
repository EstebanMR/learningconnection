<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Score extends Model
{
    protected $table = 'score';
    protected $hidden = ['id'];
    public $timestamps = false;
    protected $fillable = ['id', 'id_material', 'id_usuario', 'points'];
}

