<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


class student extends Model
{
    protected $table = 'student';
    protected $hidden = ['id'];
    public $timestamps = false;
    protected $fillable = ['id', 'user_id','grade', 'state'];
 
}
