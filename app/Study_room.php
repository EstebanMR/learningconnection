<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class study_room extends Model
{
    protected $table = 'study_room';
    protected $hidden = ['id'];
    public $timestamps = false;
    protected $fillable = ['id', 'name', 'creator', 'date_room', 'time_room', 'subject_id', 'content_id', 'content1_id', 'content2_id', 'grade_id', 'max_useres', 'state'];
}
