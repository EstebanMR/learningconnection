<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    
    protected $table = 'subject';
    protected $hidden = ['id'];
    public $timestamps = false;
    protected $fillable = ['id', 'subject', 'active', 'state'];
}
