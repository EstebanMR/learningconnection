<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class teacher extends Model
{
    protected $table = 'teacher';
    protected $hidden = ['id'];
    public $timestamps = false;
    protected $fillable = ['id', 'user_id', 'institute_focus', 'cicle_focus', 'subject_focus','active', 'state'];
}
