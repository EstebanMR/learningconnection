<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users_room extends Model
{
    protected $table = 'users_room';
    protected $hidden = ['id'];
    public $timestamps = false;
    protected $fillable = ['id', 'position', 'id_room', 'id_user', 'max_users', 'state'];
}
