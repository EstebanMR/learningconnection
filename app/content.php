<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class content extends Model
{
    protected $table = 'content';
    protected $hidden = ['id'];
    public $timestamps = false;
    protected $fillable = ['id', 'subject_id','grade_id', 'content', 'active', 'state'];
    
    public function subject()
    {
        return $this->belongsTo('App\Subject');
    }

    public function grade()
    {
        return $this->belongsTo('App\Grade');
    }
}
