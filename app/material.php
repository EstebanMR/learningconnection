<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class material extends Model
{
    protected $table = 'material';
    protected $hidden = ['id'];
    public $timestamps = false;
    protected $fillable = ['id', 'user_id', 'link', 'name', 'grade_id','subject_id', 'content_id', 'points', 'type', 'process', 'active', 'state'];
}
