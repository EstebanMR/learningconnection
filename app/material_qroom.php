<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class material_qroom extends Model
{
    protected $table = 'materials_qroom';
    protected $hidden = ['id'];
    public $timestamps = false;
    protected $fillable = ['id', 'id_room', 'id_material'];
}
