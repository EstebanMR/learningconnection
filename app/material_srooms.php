<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class material_srooms extends Model
{
    protected $table = 'materials_room';
    protected $hidden = ['id'];
    public $timestamps = false;
    protected $fillable = ['id', 'id_room', 'id_material'];
}
