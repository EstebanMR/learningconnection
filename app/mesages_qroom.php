<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mesages_qroom extends Model
{
    protected $table = 'mesages_qroom';
    protected $hidden = ['id'];
    public $timestamps = false;
    protected $fillable = ['id', 'id_room', 'id_user', 'message', 'date_sms', 'time_sms'];
}
