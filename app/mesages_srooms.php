<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mesages_srooms extends Model
{
    protected $table = 'mesages_sroom';
    protected $hidden = ['id'];
    public $timestamps = false;
    protected $fillable = ['id', 'id_room', 'id_user', 'message', 'date_sms', 'time_sms'];
}
