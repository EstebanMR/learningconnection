@extends('layouts.app')


@section('content')
<style>
    h3, h4, p, span, input, label, option, select, strong{
        color:#0D5DA7;

    }

    .blue:hover{
        color:white; 
        background-color: #F1B809;
        text-decoration: none;
        font-family: 'Nunito';
    }

    .blue{
        color:white; 
        background-color: #0D5DA7;
        text-decoration: none;
        font-family: 'Nunito';
    }

</style>
<div class="m-5">
    <div class="title mx-5"><h3><strong  style="font-family: 'Nunito';">SOLICITUDES</strong></h3></div>
    <div class="accordion mt-5" id="accordion">
        <div class="card">
            <div class="card-header" id="headingOne">
                <h4 class="mb-0">
                    <button class="btn btn-link btn-block text-left ml-2" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        <strong>MATERIAL DIDÁCTICO</strong><img class="ml-3" src="https://192.168.1.109/img/arrowDown.svg" height="10vh" alt="">
                    </button>
                </h4>
            </div>
            <div style="height:45vh; overflow-y:auto;" class="collapse show" id="collapseOne" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Usuario</th>
                                <th scope="col">link</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Materia</th>
                                <th scope="col">Nivel</th>
                                <th scope="col">Contenido</th>
                                <th scope="col">Puntos</th>
                                <th scope="col">Tipo</th>
                                <th scope="col">Procesado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $index=0;
                            ?>
                            @foreach($materials as $material)
                                <?php
                                    $index++;
                                    $subjectId;
                                    $subjectName;
                                    $gradeId;
                                    $gradeName;
                                    $contentId;
                                    $contentName;
                                    $userName;
                                    foreach ($subjects as $subject) {
                                        if ($subject->id === $material->subject_id) {
                                            $subjectId=$subject->id;
                                            $subjectName=$subject->subject;
                                        }
                                    }
                                    foreach ($grades as $grade) {
                                        if ($grade->id === $material->grade_id) {
                                            $gradeId=$grade->id;
                                            $gradeName=$grade->grade_name;
                                        }
                                    }
                                    foreach ($contents as $content) {
                                        if ($content->id === $material->content_id) {
                                            $contentId=$content->id;
                                            $contentName=$content->content;
                                        }
                                    }
                                    foreach ($users as $user) {
                                        if ($user->id === $material->user_id) {
                                            $userName=$user->username;
                                        }
                                    }

                                ?>
                                <tr>
                                    <td>{{ $index }}</td>
                                    <td>{{ $userName }}</td>
                                    <td><a @if(str_contains($material->link, "public/material")) href="{{ Storage::url($material->link) }}" @else href="{{ $material->link }}" @endif > <?php if(str_contains($material->link, "public/material")) {$link = explode("/", $material->link); echo($link[2]); } else { echo($material->link); }?> </a></td>
                                    <td>{{ $material->name }}</td>
                                    <td>{{ $subjectName }}</td>
                                    <td>{{ $gradeName }}</td>
                                    <td>{{ $contentName }}</td>
                                    <td>{{ $material->points }}</td>
                                    <td>{{ $material->type }}</td>
                                    <td>{{ $material->process }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div style="width:20vw; margin-left:1.3vw;">
                        <span><strong>Solicitud de Materia Didáctico</strong></span>
                        <br>
                        <br>
                        <form action="{{ route('createrequests') }}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label for="" class="col-form-label"><span><strong>Tipo</strong></span><select style="margin-left:3vw; width:8.7vw;" name="type" id="type" class="form-select" onchange="return showfields();">
                                    <option selected>Seleccione una opción...</option>
                                    <option value="PDF">PDF</option>
                                    <option value="Documento de texto">Documento de texto</option>
                                    <option value="Imagen">Imagen</option>
                                    <option value="Video">Video</option>
                                    <option value="Enlace de PDF web">Enlace de PDF web</option>
                                    <option value="Enlace de documento web">Enlace de documento web</option>
                                    <option value="Enlace de pagina web">Enlace de pagina web</option>
                                    <option value="Enlace de imagen web">Enlace de imagen web</option>
                                    <option value="Enlace de video web">Enlace de video web</option>
                                </select></label>
                            </div>
                                
                            <div id = "web"  class="form-group row">
                                <label for="" class="col-form-label"><span><strong>Link</strong></span><input style="margin-left:3.2vw; width:8.7vw;" id="link" type="text" name="link"></label>
                            </div>

                            <div id = "local" class="form-group row" style="visibility:hidden;">
                                <label for="" class="col-form-label"><span><strong>Archivo</strong></span><input style="margin-left:2vw; width:8.7vw;" id="doc" type="file" name="doc"></label>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-form-label"><span><strong>Nombre</strong></span><input style="margin-left:2vw; width:8.7vw;" id="name" type="text" name="name" ></label>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-form-label"><span><strong>Materia</strong></span><select style="margin-left:2vw; width:8.7vw;" name="subject" id="subject" class="form-select">
                                    <option selected>Seleccione una opción...</option>
                                    @foreach($subjects as $subject)
                                        <option value="{{ $subject->id }}">{{ $subject->subject }}</option>
                                    @endforeach
                                </select></label>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-form-label"><span><strong>Nievel</strong></span><select style="margin-left:2.5vw; width:8.7vw;" name="grade" id="grade" class="form-select">
                                    <option selected>Seleccione una opción...</option>
                                    @foreach($grades as $grade)
                                        <option value="{{ $grade->id }}">{{ $grade->grade_name }}</option>
                                    @endforeach
                                </select></label>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-form-label"><span><strong>Contenido</strong></span><select style="margin-left:1.2vw; width:8.7vw;" name="content" id="content" class="form-select">
                                    <option selected>Seleccione una opción...</option>
                                    @foreach($contents as $content)
                                        <option value="{{ $content->id }}">{{ $content->content }}</option>
                                    @endforeach
                                </select></label>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-form-label"><span><strong>Puntos</strong></span><input style="margin-left:2.3vw; width:8.7vw;" id="points" type="text" name="points" disabled></label>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-form-label"><span><strong>Procesado</strong></span><input style="margin-left:1.2vw; width:8.7vw;" id="process" type="text" name="process" disabled></label>
                            </div>
                            <br>
                            <div class="form-group text-center">
                                <input type="submit" class="btn blue" name="submit" value="ENVIAR SOLICITUD">
                            </div>
                        </form> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function showfields(){
        var dropdown = document.getElementById('type');
        var opt = dropdown.options [dropdown.selectedIndex].value;
        if (opt == "PDF" || opt == "Documento de texto" || opt == "Imagen" || opt == "Video" ) {
            document.getElementById('local').style.visibility = 'visible';
            document.getElementById('web').style.visibility = 'hidden';
        } else {
            document.getElementById('local').style.visibility = 'hidden';
            document.getElementById('web').style.visibility = 'visible';
        }
    }
</script>
@endsection