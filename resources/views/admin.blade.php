@extends('layouts.app')

@section('content')
<div class="row d-flex justify-content-center" style="margin-top:2vw; margin-bottom:4vw; width:100%;">
    <h3><strong>DASHBOARD DE INFORMACION</strong></h3>
</div>
<div class="row" style="margin-left:7vw; margin-right:2vw; margin-button:7vw; height:65vh">
    <div class="col-md-3 " style="height:55vh; margin-right: 3vw">
        <canvas id="myChart" width="400" height="400"></canvas>
        <?php
            echo("<script>
            const ctx = document.getElementById('myChart').getContext('2d');
            const myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: [".$teachers."],
                    datasets: [{
                        label: 'Cantidad aportada',
                        data: [".$materials."],
                        backgroundColor: [".$color1."],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        x: {
                            ticks: {
                                color: '#0D5DA7',
                            }
                        }
                    },
                    responsive: true,
                    plugins: {
                        datalabels: {
                            color: '#0D5DA7'
                        },
                        legend: {
                            labels: {
                                color: '#0D5DA7'
                            },
                            position: 'top',
                        },
                        title: {
                            display: true,
                            text: 'Usuarios con mayor cantidad de material didáctico aportado',
                            color:'#0D5DA7'
                        }
                    }
                }
            });
            </script>")
        ?>

    </div>
    <div class="col-md-1"></div>
    <div class="col-md-3" style="height:55vh;">
        <canvas id="Chart1" width="400" height="400"></canvas>
        <?php
            echo("<script>
            const ctx1 = document.getElementById('Chart1').getContext('2d');
            const myChart1 = new Chart(ctx1, {
                type: 'doughnut',
                data: {
                    labels: ['Docentes', 'Estudiantes'],
                    datasets: [
                      {
                        label: 'Dataset 1',
                        data: [".$users."],
                        backgroundColor: [
                            '#E62C28',
                            '#0D5DA7'
                        ],
                      }
                    ]
                },
                options: {
                    responsive: true,
                    plugins: {
                        legend: {
                            labels: {
                                color: '#0D5DA7'
                            },
                            position: 'top',
                        },
                        title: {
                            display: true,
                            text: 'Cantidad de usuarios por tipo',
                            color:'#0D5DA7'
                        }
                    }
                },
            });
            </script>")
        ?>

    </div>
    <div class="col-md-1"></div>
    <div class="col-md-3" style="height:55vh;">
        <canvas id="Chart2" width="400" height="400"></canvas>
        <?php
            echo("<script>
            const ctx2 = document.getElementById('Chart2').getContext('2d');
            const myChart2 = new Chart(ctx2, {
                type: 'line',
                data: {
                    labels: [".$term."],
                    datasets: [
                      {
                        label: 'Salas de estudio activas',
                        data: [".$rooms."],
                        fill:false,
                        backgroundColor: '#E62C28',
                        borderColor: '#0D5DA7',
                        tension:0
                      }
                    ]
                },
                options: {
                    scales: {
                        x: {
                            ticks: {
                                color: '#0D5DA7',
                            }
                        },
                        y: {
                            beginAtZero: true
                        }
                    },
                    responsive: true,
                    plugins: {
                        legend: {
                            labels: {
                                color: '#0D5DA7'
                            },
                            position: 'top',
                        },
                        title: {
                            display: true,
                            text: 'Salas de estudio activas por grado y materia',
                            color:'#0D5DA7'
                        }
                    }
                },
            });
            </script>")
        ?>

    </div>
</div>
@endsection