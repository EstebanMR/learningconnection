<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

        <title>Laravel</title>

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: sans-serif;
                font-weight: 10;
                height: 70vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-weight: 900;
                font-size: 1.3rem; 
                color:#0D5DA7
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            input, button, h1, h2, h3{
                font-family: 'Nunito';
                margin:0px;
            }

            .in{
                width:100%;
                margin-top:0.5rem;
                margin-bottom:1rem;
            }

            .abs-center {
                margin-top:5rem;
                display:flex;
                align-items: center;
                justify-content: center;
                min-height: 40vh;
            }


            .blue:hover{
                color:white; 
                background-color: #F1B809;
                text-decoration: none;
                font-family: 'Nunito';
            }

            .blue{
                color:white; 
                background-color: #0D5DA7;
                text-decoration: none;
                font-family: 'Nunito';
            }

            .abs-center-nm {
                margin-top:0.3rem;
                display:flex;
                align-items: center;
                justify-content: center;
            }

            .btnblue{
                background-color: #0D5DA7; 
                color: white; 
                font-family: 'Nunito'; 
                font-size:0.9vw; 
                border-radius: 7px; 
                height: 3vh; 
                width: 7vw;
            }
            .im1{
                background-image:url(https://192.168.1.109/img/chain.svg);
                background-repeat:no-repeat; 
                background-position:1% center;
                background-size:5.5%;
                padding-left: 10%;
                border:2px solid lightgrey;
                height: 3vh;
                font-size:90%;
            }

            .im{
                background-image:url(https://192.168.1.109/img/user1.svg);
                background-repeat:no-repeat; 
                background-position:1% center;
                background-size:7%;
                padding-left: 10%;
                border:2px solid lightgrey;
                height: 3vh;
                font-size:90%;
            }
        </style>
    </head>
    <body>
        <div>
            @if (Route::has('login'))
                <div class="px-1">
                    <div class="position-absolute top-100 start-100 translate-middle" style="margin-top:1%; padding-left: 2%; width:15vw">
                    <a href="{{ route('home') }}"><img src="https://192.168.1.109/img/Isotipo.svg" height="45vh"><img src="https://192.168.1.109/img/letras.svg" height="25vh" style="padding-left:0.5rem;"></a>
                    </div>
                    <div class="top-right links">
                        <a style="color:#0D5DA7; font-family: 'Nunito'" href="{{ url('/home') }}"><h3><bold>QUIENES SOMOS LEARNING CONNECTION?</bold></h3></a>
                    </div>
                </div>
            @endif
            <div class="abs-center">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-body">
                                <div class="row justfy-content-center" style="text-align: center;">:
                                    <div class="col-4">
                                        <img src="https://192.168.1.109/img/user1.svg" height="150vh">
                                    </div>
                                </div>
                                <br>
                                <br>
                                <br>
                                <form method="POST" action="{{ route('login') }}">
                                    @csrf

                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <input placeholder="Correo" id="email" type="email" class="im in form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <input placeholder="Contraseña" id="password" type="password" class="im1 in form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row mb-0">
                                        <div class="col-md-8 offset-md-4" style="width:15vw">
                                            <div class="abs-center-nm">
                                                <button style="width:7vw; height:4vh; border-radius:10px; font-size:1.2rem" type="submit" class="in btn blue">
                                                    {{ __('ACEPTAR') }}
                                                </button>
                                            </div>
                                            @if (Route::has('password.request'))
                                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                                    {{ __('Olvidaste la contraseña?') }}
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bkgn" style="height:0%">
            <img style="position:relative; overflow: hidden; bottom: 22vh; left:14vw" src="https://192.168.1.109/img/Ellipse5.svg" height="130vh">
            <img style="position:relative; overflow: hidden; bottom: 37vh; left:16vw" src="https://192.168.1.109/img/Ellipse5.svg" height="80vh">
            <img style="position:relative; overflow: hidden; bottom: 15vh; left:81.2vw" src="https://192.168.1.109/img/Ellipse4.svg" height="320vh">
        </div>
    </body>
</html>