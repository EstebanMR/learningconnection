@extends('layouts.app')

@section('content')

<style>
    h2, h4, input, th, td, p, span, input, label, option, select{
        color:#0D5DA7;

    }
    .blue:hover{
        color:white; 
        background-color: #F1B809;
        text-decoration: none;
        font-family: 'Nunito';
    }

    .blue{
        color:white; 
        background-color: #0D5DA7;
        text-decoration: none;
        font-family: 'Nunito';
    }

    .red:hover{
        color:white; 
        background-color: #F1B809;
        text-decoration: none;
        font-family: 'Nunito';
    }

    .red{
        color:white; 
        background-color: #E62C28EE;
        text-decoration: none;
        font-family: 'Nunito';
    }
</style>

<div class="container" style="height: 73vh;">
    <div class="row justify-content-center" style="height:100%;">
        <div class="col-md-8" style="height:100%;">
            <div class="card" style="height:100%;">

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    
                    <h2>Niveles</h2>
                    <button type="button" class="btn blue" data-toggle="modal" data-target="#createModal">Crear Nivel</button>

                    <br>
                    <br>
                    <div class=" scroll1 table-responsive" style="height:85%; overflow-y:auto">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Numero</th>
                                    <th scope="col">Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $index=0;
                                ?>
                                @foreach($grades as $grade)
                                    <?php
                                        $index++;
                                    ?>
                                    <tr>
                                        <td>{{ $index }}</td>
                                        <td>{{ $grade->grade_name }}</td>
                                        <td>{{ $grade->grade_number }}</td>
                                        <td>@if($grade->active === 1) 
                                            <button type="button" class="btn blue" data-toggle="modal" data-target="#createModal" onclick="document.getElementById('id').value='{{$grade->id}}'; document.getElementById('name').value='{{$grade->grade_name}}'; document.getElementById('grade').value='{{$grade->grade_number}}';">Editar</button>
                                            <button onclick="document.getElementById('id1').value='{{$grade->id}}';  event.preventDefault(); document.getElementById('delete-form').submit();" class="btn red" type="button">Delete</button>
                                            <form id="delete-form" action="{{ route('deleteGrade', $grade->id) }}" method="post" style="display: none;">
                                                @csrf
                                                @method('DELETE')
                                                <input id="id1" type="number" name="id1" hidden>
                                            </form>
                                            @else false @endif</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="createModal" role="dialog">
    <div class="modal-dialog">
    
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Crear Nivel</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <form action="{{ route('createGrade') }}" method="post">
                        @csrf

                        <div class="form-group row">
                            <input id="id" type="number" name="id" hidden>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-form-label"><span>Nombre:</span><input id="name" type="text" name="name" ></label>
                        </div>

                        <div class="form-group row">
                            <label style="padding-left:1rem"></label>
                            <label for="grade" class="col-form-label"><span>Nivel:</span><input id="grade" type="number" name="grade" autofocus min="1" max="12"></label>
                        </div>

                        <div class="form-group text-center">
                            <input type="submit" class="btn blue" name="submit" value="Guardar">
                        </div>

                    </form>  
                </div>
            </div>
        </div>
    </div>
</div>

@endsection