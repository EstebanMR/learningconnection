<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Learning Connection</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://js.pusher.com/4.0/pusher.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.min.js" integrity="sha384-VHvPCCyXqtD5DqJeNxl2dtTyhF78xXNXdkwX1CZeRusQfRKp+tA7hAShOK/B/fQ2" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@3.6.0/dist/chart.min.js"></script>

    @livewireStyles
    @livewireScripts

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        h4, h5, h3{
            color:#0D5DA7;
        }
        .btn{
            font-family: 'Nunito';
            font-size: 1rem
        }
        .flex-center {
            display: flex;
            justify-content: center;
        }
        span, p{
            font-family: "Open Sans";
        }
        .ref{
            color:#0D5DA7; 
            font-family: 'Nunito';
            padding: 0.7rem;
            font-weight: bold;
            font-size: 1.1rem;
            border-radius: 7px
        }

        .ref:hover{
            color:#F1B809; 
            text-decoration: none;
            font-family: 'Nunito';
            padding: 0.7rem;
            font-weight: bold;
            font-size: 1.1rem;
        }

        .active{
            color:#E62C28; 
            text-decoration: none;
            font-family: 'Nunito';
            padding: 0.7rem;
            font-weight: bold;
            font-size: 1.1rem;
        }

        .active:hover{
            color:#F1B809; 
            text-decoration: none;
            font-family: 'Nunito';
            padding: 0.7rem;
            font-weight: bold;
            font-size: 1.1rem;
        }

        .active{
            color:#E62C28; 
            text-decoration: none;
            font-family: 'Nunito';
            padding: 0.7rem;
            font-weight: bold;
            font-size: 1.1rem;
        }

        .active:hover{
            color:#F1B809; 
            text-decoration: none;
            font-family: 'Nunito';
            padding: 0.7rem;
            font-weight: bold;
            font-size: 1.1rem;
        }

        .inicio{
            padding-top: 0.7rem;
            padding-left: 0.5rem;
        }

        .inicio:hover{
            color:#F1B809;
            padding-top: 0.7rem;
            padding-left: 0.5rem;
        }

        .img:hover{
            content:url(https://192.168.1.109/img/inicio1.svg);
        }
        .scroll1::-webkit-scrollbar {
        width: 5px;     
        }
        .scroll1::-webkit-scrollbar-thumb {
            background-color: lightgrey;  
            width:3px;
            border-radius: 10px;    
            margin-right:0.3rem;     
        }
    </style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <a style="margin-left:3.3vw" href="{{ route('home') }}"><img src="https://192.168.1.109/img/Isotipo.svg" height="45vh"><img src="https://192.168.1.109/img/letras.svg" height="25vh" style="padding-left:0.5rem"></a>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav mr-5">
                        <!-- Authentication Links -->
                        @guest
                        @else
                            @if(Auth::user()->type === 1)
                                <a class="<?php if($_SERVER['REQUEST_URI']=='/grade') {echo "active";} else {echo "ref";} ?>" href="{{ route('grade') }}">NIVELES</a>
                                <a class="<?php if($_SERVER['REQUEST_URI']=='/material') {echo "active";} else {echo "ref";} ?>" href="{{ route('material') }}">MATERIAL DIDÁCTICO</a>
                                <a class="<?php if($_SERVER['REQUEST_URI']=='/subject') {echo "active";} else {echo "ref";} ?>" href="{{ route('subject') }}">ASIGNATURAS</a>
                                <a class="<?php if($_SERVER['REQUEST_URI']=='/teacher') {echo "active";} else {echo "ref";} ?>" href="{{ route('teacher') }}">DOCENTES</a>
                                <a class="<?php if($_SERVER['REQUEST_URI']=='/content') {echo "active";} else {echo "ref";} ?>" href="{{ route('content') }}">CONTENIDOS</a>
                            @elseif(Auth::user()->type === 2)
                                <a class="<?php if($_SERVER['REQUEST_URI']=='/questionRoom') {echo "active";} else {echo "ref";} ?>" href="{{ route('questionRoom') }}">CONSULTAS</a>
                                <a class="<?php if($_SERVER['REQUEST_URI']=='/teacherrequests') {echo "active";} else {echo "ref";} ?>" href="{{ route('teacherrequests') }}">SOLICITUDES</a>
                                <a class="<?php if(str_contains($_SERVER['REQUEST_URI'],'materialUser')) {echo "active";} else {echo "ref";} ?>" href="{{ route('materialUser', ['grade' => 0,'subject' =>  0, 'content' => 0, 'type' => 0]) }}">MATERIAL DIDÁCTICO</a>
                                <a class="<?php if($_SERVER['REQUEST_URI']=='/profile') {echo "active";} else {echo "ref";} ?>" href="{{ route('profile') }}">PERFIL</a>
                            @elseif(Auth::user()->type === 3)
                                <a class="<?php if($_SERVER['REQUEST_URI']=='/studyRoom') {echo "active";} else {echo "ref";} ?>" href="{{ route('studyRoom') }}" href="#">SALAS DE ESTUDIO</a>
                                <a class="<?php if(str_contains($_SERVER['REQUEST_URI'],'materialUser')) {echo "active";} else {echo "ref";} ?>" href="{{ route('materialUser', ['grade' => 0,'subject' =>  0, 'content' => 0, 'type' => 0]) }}">MATERIAL DIDÁCTICO </a>
                                <a class="<?php if($_SERVER['REQUEST_URI']=='/questionRoom') {echo "active";} else {echo "ref";} ?>" href="{{ route('questionRoom') }}">CONSULTAS</a>
                                <a class="<?php if($_SERVER['REQUEST_URI']=='/profile') {echo "active";} else {echo "ref";} ?>" href="{{ route('profile') }}">PERFIL</a>
                            @endif
                            <a class="inicio" href="{{route('logout')}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <span> @yield('user')</span>
                                <img class="img" src="https://192.168.1.109/img/inicio.svg" height="17vh">
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        <div style=" height: 4vh; weight:100%; background: -prefix-linear-gradient(top, drarkgrey, white); background: linear-gradient(to bottom, darkgrey, white);"></div>

        <main>
            @include('flash-message')

            @yield('content')
        </main>
    </div>
    <footer style="width:100%; height:9vh; background-color: #0D5DA7; position:absolute; bottom:0px; padding: 1.3vh; padding-left:4vw;">
        <img src="https://192.168.1.109/img/logoB.svg" height="60vh"><img src="https://192.168.1.109/img/letrasB.svg" height="40vh" style="padding-left:0.5rem">
    </footer>
</body>
</html>
