<div id="chat" class="scroll pr-1" style="height:90%; overflow-y:auto">
    @foreach($mensajes as $mensaje)
    <div >
        @if($usuario === $mensaje['usuario']) 
            <div style="font-family:sans-serif; margin-left:50%">
                <br>
                <strong class="name float-right">{{$mensaje["usuario"]}}</strong>
                <br>
                <p class="sms p-2 px-3">{{$mensaje["mensaje"]}}</p>
                <span class="time float-right">{{$mensaje["hora"]}}</span>
            </div>
        @elseif($usuario != $mensaje['usuario'])
            <div style="font-family:sans-serif; margin-right:50%" >
                <br>
                <strong class="name float-left">{{$mensaje["usuario"]}}</strong>
                <br>
                <p class="sms p-2 px-3">{{$mensaje["mensaje"]}}</p>
                <span class="time float-left">{{$mensaje["hora"]}}</span>
            </div>
        @endif
    <script>
        scrollDown();
    </script>
    </div>
    @endforeach
    <script>

        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = true;

        var pusher = new Pusher('79ee4372635890157767', {
            cluster: 'us2'
        });

        var channel = pusher.subscribe('chat-channel');

        channel.bind('chat-event', function(data) {
        //alert(JSON.stringify(data));
        window.livewire.emit('mensajeEnviado', data);
        });
        function scrollDown(){
            var chat = document.getElementById('chat');
            chat.scrollTop = chat.scrollHeight;
        };
        onload = scrollDown();

    </script>
</div>