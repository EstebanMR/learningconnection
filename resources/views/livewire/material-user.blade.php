<div class="col-md-4" style="height:73vh;">
    <h2 style="margin-top:5vh"><strong class="title">MATERIAL DIDÁCTICO</strong></h2>
    <br>
    <div class="col-md-11" style="height:40vh; border: 3px solid darkgrey; border-radius: 5px; padding-left:3vw">
            <br>
            <br>
            <div class="form-group">
                <label style="width:6vw" for="GRADE"><h5><strong>NIVEL</strong></h5></label>
                <select style="width:9vw; border-radius:5px" name="grade" id="grade"class="form-select" wire:model.lazy="grade" wire:change="filter">
                    <option value="0" selected>Seleccione Nivel</option>
                        @foreach ($grades as $grade)
                            <option value="{{ $grade->id }}">{{ $grade->grade_number }}</option>
                        @endforeach
                </select>
            </div>
            <div class="form-group">
                <label style="width:6vw" for="subject"><h5><strong>ASIGNATURA</strong></h5></label>
                <select style="width:9vw; border-radius:5px" name="subject" id="subject" class="form-select" wire:model="subject" wire:change="filter">
                    <option value="0" selected>Seleccione Asignatura</option>
                        @foreach ($subjects as $subject)
                            <option value="{{ $subject->id }}">{{ $subject->subject }}</option>
                        @endforeach
                </select>
            </div>
            <div class="form-group">
                <label style="width:6vw" for=""><h5><strong>CONTENIDO</strong></h5></label>
                <select style="width:9vw; border-radius:5px" name="content" id="content" class="form-select" wire:model="content" wire:change="filter">
                    <option value="0" selected>Seleccione Contenido</option>
                        @foreach ($contents as $content)
                            <option value="{{ $content->id }}">{{ $content->content }}</option>
                        @endforeach
                </select>
            <div class="form-group">
                <label style="width:6vw" for=""><h5><strong>TIPO</strong></h5></label>
                <select style="width:9vw; border-radius:5px" name="type" id="type" class="form-select" wire:model="type" wire:change="filter">
                    <option value="0" selected>Seleccione Tipo</option>
                    @foreach ($types as $type)
                            <option value="{{ $type }}">{{ $type }}</option>
                        @endforeach
                </select>
            </div>
    </div>
</div>
</div>
<div class="scroll col-md-8 " style="height:61vh; margin-top:12vh; overflow-y:auto; display:flex; flex-direction:column; align-items:center">
    @foreach ($this->materials as $material)
        <div style="height:10vh; width:85%; border: 3px solid darkgrey; border-radius: 7px; margin-bottom:3vh; padding:1vw; display:flex; flex-direction:row; ">
            <a style="text-decoration:none;" @if(str_contains($material->link, "public/material")) href="{{ Storage::url($material->link) }}" @else href="{{ $material->link }}" @endif><h5 style="width:27vw"><strong>{{$material->name}}</strong></h5></a>
            <span style="margin-top:1vh">Te gustó?</span>
            <form style="padding-left:1vw; margin-top:1vh" action="{{ route('materialU') }}" method="post" >
                @csrf
                <input id="id1" type="number" name="id1" hidden value="{{$material->id}}">
                <select name="points" id="points" class="form-select" style="border-radius:5px" onchange="this.form.submit();">
                    <option value="0" selected >0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
            </form>
            <div class="column" style="margin-left:2vw; margin-top:1vh">
                <div class="row" style="margin-left:0vw;">
                <?php $n=1;?>
                @for($i = 0; $i <= 5; $i++)
                    @if($n<=$material->points)
                        <img src="https://192.168.1.109/img/StarFill.svg" width="20vh" alt="">   
                    @elseif($n>$material->points)
                        <img src="https://192.168.1.109/img/Star.svg" width="20vh" alt="">
                    @endif
                    <?php $n++;?>
                @endfor
                </div>
                <span>{{$material->points}}.0</span>
            </div>
        </div>
    @endforeach
</div>