<div>
    <div class="sender p-2">
        <input style="display:none" type="numeric" id="id" name="id" value="{{$sala}}">
        <input class="sinborde" style="font-family:sans-serif; width:90%;" type="text" name="mensaje" id="mensaje" placeholder="Mensaje" wire:model="mensaje" maxlength="550">
        <button class="btn" onclick="return limpiar()" wire:click="enviarMensaje"><img src="https://192.168.1.109/img/send.svg" height="20vh" alt=""></button>
        <br>@error('mensaje') <small class="text-danger">{{$message}}</small> @enderror
    </div>
</div>
<script>
    function limpiar(){
        document.getElementById('mensaje').value='';
    }
</script>