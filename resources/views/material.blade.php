@extends('layouts.app')

@section('content')

<style>
    h2, h4, input, th, td, p, span, input, label, option, select{
        color:#0D5DA7;

    }

    
    .blue:hover{
        color:white; 
        background-color: #F1B809;
        text-decoration: none;
        font-family: 'Nunito';
    }

    .blue{
        color:white; 
        background-color: #0D5DA7;
        text-decoration: none;
        font-family: 'Nunito';
    }

    .red:hover{
        color:white; 
        background-color: #F1B809;
        text-decoration: none;
        font-family: 'Nunito';
    }

    .red{
        color:white; 
        background-color: #E62C28EE;
        text-decoration: none;
        font-family: 'Nunito';
    }
</style>

<div class="container" style="height: 73vh;">
    <div class="row justify-content-center" style="height:100%;">
        <div class="col-md-8" style="height:100%;">
            <div class="card" style="height:100%;">

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    
                    <h2>Material Didáctico</h2>
                    <button type="button" class="btn blue" data-toggle="modal" data-target="#createModal">Crear Contenido</button>

                    <br>
                    <br>
                    <div class="scroll1 table-responsive" style="height:85%; overflow-y:auto">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Usuario</th>
                                    <th scope="col">link</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Materia</th>
                                    <th scope="col">Nivel</th>
                                    <th scope="col">Contenido</th>
                                    <th scope="col">Puntos</th>
                                    <th scope="col">Tipo</th>
                                    <th scope="col">Procesado</th>
                                    <th scope="col">Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $index=0;
                                ?>
                                @foreach($materials as $material)
                                    <?php
                                        $index++;
                                        $subjectId;
                                        $subjectName;
                                        $gradeId;
                                        $gradeName;
                                        $contentId;
                                        $contentName;
                                        $userName;
                                        foreach ($subjects as $subject) {
                                            if ($subject->id === $material->subject_id) {
                                                $subjectId=$subject->id;
                                                $subjectName=$subject->subject;
                                            }
                                        }
                                        foreach ($grades as $grade) {
                                            if ($grade->id === $material->grade_id) {
                                                $gradeId=$grade->id;
                                                $gradeName=$grade->grade_name;
                                            }
                                        }
                                        foreach ($contents as $content) {
                                            if ($content->id === $material->content_id) {
                                                $contentId=$content->id;
                                                $contentName=$content->content;
                                            }
                                        }
                                        foreach ($users as $user) {
                                            if ($user->id === $material->user_id) {
                                                $userName=$user->username;
                                            }
                                        }

                                  ?>
                                    <tr>
                                        <td>{{ $index }}</td>
                                        <td>{{ $userName }}</td>
                                        <td><a @if(str_contains($material->link, "public/material")) href="{{ Storage::url($material->link) }}" @else href="{{ $material->link }}" @endif > <?php if(str_contains($material->link, "public/material")) {$link = explode("/", $material->link); echo($link[2]); } else { echo($material->link); }?> </a></td>
                                        <td>{{ $material->name }}</td>
                                        <td>{{ $subjectName }}</td>
                                        <td>{{ $gradeName }}</td>
                                        <td>{{ $contentName }}</td>
                                        <td>{{ $material->points }}</td>
                                        <td>{{ $material->type }}</td>
                                        <td>{{ $material->process }}</td>
                                        <td>@if($material->active === 1)
                                            <button type="button" class="btn blue" data-toggle="modal" data-target="#createModal" onclick="document.getElementById('id').value='{{$material->id}}'; document.getElementById('user').value='{{$userName}}'; document.getElementById('link').value='{{$material->link}}'; document.getElementById('content').value='{{$contentId}}'; document.getElementById('subject').value='{{$subjectId}}'; document.getElementById('grade').value='{{$gradeId}}'; document.getElementById('content').value='{{$contentId}}'; document.getElementById('name').value='{{$material->name}}'; document.getElementById('points').value='{{$material->points}}'; document.getElementById('type').value='{{$material->type}}'; document.getElementById('process').value='{{$material->process}}';">Editar</button>
                                            <button onclick="document.getElementById('id1').value='{{$material->id}}';  event.preventDefault(); document.getElementById('delete-form').submit();" class="btn red" type="button">Delete</button>
                                            <form id="delete-form" action="{{ route('deleteMaterial', $material->id) }}" method="post" style="display: none;">
                                                @csrf
                                                @method('DELETE')
                                                <input id="id1" type="number" name="id1" hidden>
                                            </form>
                                            @else 
                                                <button onclick="document.getElementById('idt').value='{{$material->id}}';  event.preventDefault(); document.getElementById('accept-form').submit();" class="btn blue" type="button">Aceptar</button>
                                                <form id="accept-form" action="{{ route('acceptMaterial', $material->id) }}" method="post" style="display: none;">
                                                    @csrf
                                                    <input id="idt" type="number" name="idt" hidden>
                                                </form>
                                                <button onclick="document.getElementById('id1').value='{{$material->id}}';  event.preventDefault(); document.getElementById('delete-form').submit();" class="btn red" type="button">Delete</button>
                                                <form id="delete-form" action="{{ route('deleteMaterial', $material->id) }}" method="post" style="display: none;">
                                                    @csrf
                                                    @method('DELETE')
                                                    <input id="id1" type="number" name="id1" hidden>
                                                </form>
                                            @endif</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="createModal" role="dialog">
        <div class="modal-dialog">
        
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Crear Material</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <form action="{{ route('createMaterial') }}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group row">
                                <input id="id" type="number" name="id" hidden>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-form-label"><span>Usuario:</span><input id="user" type="text" name="user" disabled></label>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-form-label"><span>Tipo:</span><select name="type" id="type" class="form-select" onchange="return showfields();">
                                    <option selected>Seleccione una opción...</option>
                                    <option value="PDF">PDF</option>
                                    <option value="Documento de texto">Documento de texto</option>
                                    <option value="Imagen">Imagen</option>
                                    <option value="Video">Video</option>
                                    <option value="Enlace de PDF web">Enlace de PDF web</option>
                                    <option value="Enlace de documento web">Enlace de documento web</option>
                                    <option value="Enlace de pagina web">Enlace de pagina web</option>
                                    <option value="Enlace de imagen web">Enlace de imagen web</option>
                                    <option value="Enlace de video web">Enlace de video web</option>
                                </select></label>
                            </div>
                            
                            <div id = "web"  class="form-group row">
                                <label for="" class="col-form-label"><span>Link:</span><input id="link" type="text" name="link"></label>
                            </div>

                            <div id = "local" class="form-group row" style="visibility:hidden;">
                                <label for="" class="col-form-label"><span>Archivo:</span><input id="doc" type="file" name="doc"></label>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-form-label"><span>Nombre:</span><input id="name" type="text" name="name" ></label>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-form-label"><span>Materia:</span><select name="subject" id="subject" class="form-select">
                                    <option selected>Seleccione una opción...</option>
                                    @foreach($subjects as $subject)
                                        <option value="{{ $subject->id }}">{{ $subject->subject }}</option>
                                    @endforeach
                                </select></label>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-form-label"><span>Nievel:</span><select name="grade" id="grade" class="form-select">
                                    <option selected>Seleccione una opción...</option>
                                    @foreach($grades as $grade)
                                        <option value="{{ $grade->id }}">{{ $grade->grade_name }}</option>
                                    @endforeach
                                </select></label>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-form-label"><span>Contenido:</span><select name="content" id="content" class="form-select">
                                    <option selected>Seleccione una opción...</option>
                                    @foreach($contents as $content)
                                        <option value="{{ $content->id }}">{{ $content->content }}</option>
                                    @endforeach
                                </select></label>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-form-label"><span>Puntos:</span><input id="points" type="text" name="points" disabled></label>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-form-label"><span>Procesado:</span><input id="process" type="text" name="process" disabled></label>
                            </div>

                            <div class="form-group text-center">
                                <input type="submit" class="btn blue" name="submit" value="Guardar">
                            </div>

                        </form> 
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function showfields(){
        var dropdown = document.getElementById('type');
        var opt = dropdown.options [dropdown.selectedIndex].value;
        if (opt == "PDF" || opt == "Documento de texto" || opt == "Imagen" || opt == "Video" ) {
            document.getElementById('local').style.visibility = 'visible';
            document.getElementById('web').style.visibility = 'hidden';
        } else {
            document.getElementById('local').style.visibility = 'hidden';
            document.getElementById('web').style.visibility = 'visible';
        }
    }
</script>

@endsection