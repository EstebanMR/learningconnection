@extends('layouts.app')


@section('content')
<style>
    html, body{
        font-family: 'sans-serif';
    }
    h2, input, th, td, p, span, input, label, option, select, strong, small{
        color:#0D5DA7;

    }
    .title{
        font-family: 'Nunito';
        font-weight: bold;
    }
    .blue:hover{
        color:white; 
        background-color: #F1B809;
        text-decoration: none;
        font-family: 'Nunito';
    }

    .blue{
        color:white; 
        background-color: #0D5DA7;
        text-decoration: none;
        font-family: 'Nunito';
    }

    .red:hover{
        color:white; 
        background-color: #F1B809;
        text-decoration: none;
        font-family: 'Nunito';
    }

    .red{
        color:white; 
        background-color: #E62C28EE;
        text-decoration: none;
        font-family: 'Nunito';
    }

    .scroll::-webkit-scrollbar {
        width: 7px;         
    }

    .scroll::-webkit-scrollbar-thumb {
        background-color: lightgrey;  
        border-radius: 20px;    
        margin-right:0.3rem;     
    }

    .scroll1::-webkit-scrollbar {
        width: 5px;     
    }
    
    .scroll1::-webkit-scrollbar-thumb {
        background-color: lightgrey;  
        width:3px;
        border-radius: 10px;    
        margin-right:0.3rem;     
    }
</style>

<div class="row" style="width:100%; height:100%">
    <div class="col-md-1"></div>
    <div class="col-md-10 row" style="height:75vh;">
        @livewire("material-user", ['grade' => $grade, 'subject' =>$subject, 'content'=>$content, 'type' => $type])
    </div>
</div>
@endsection