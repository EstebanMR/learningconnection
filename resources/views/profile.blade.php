@extends('layouts.app')


@section('content')
    <style>
        .borde{
            border: 2px solid darkgrey;
            border-radius: 10px;
        }
        .bordeusuario{
            border: 2px solid darkgrey;
            border-radius: 5px;
        }
        .fact{
            color:#0D5DA7;
        }
        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
            flex-direction: column;
        }
        .blue:hover{
            color:white; 
            background-color: #F1B809;
            text-decoration: none;
            font-family: 'Nunito';
        }

        .blue{
            color:white; 
            background-color: #0D5DA7;
            text-decoration: none;
            font-family: 'Nunito';
        }
        .red:hover{
            color:white; 
            background-color: #F1B809;
            text-decoration: none;
            font-family: 'Nunito';
        }

        .red{
            color:white; 
            background-color: #E62C28EE;
            text-decoration: none;
            font-family: 'Nunito';
        }
    </style>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-5 borde" style="height:76vh;background:white;">
                <div class="card py-4 px-5 flex-center" style=" width:100%; border:white;">
                    <div class="flex-center col-md-4">
                        <img class=" mb-2" src="https://192.168.1.109/img/user1.svg" height="90vh" alt="">
                        <span class="borde fact p-1" style="font-size: 1.1rem; width: 9vw; text-align:center">{{$user->username}}</span>
                        <span class="fact">Usuario</span>
                    </div>
                    <br>
                    <div>
                        <form action="{{ route('editUser') }}" method="post" accept-charset="UTF-8" onsubmit=" document.getElementById('user').disabled = false;">
                            @csrf
                            <h4 class="fact"><strong>Datos personales</strong><img class="pl-2" src="https://192.168.1.109/img/icons_pencil.svg" height="23vh" alt=""></h4>
                            <div class="form-group mb-0">
                                <input style="width:8vw; text-align:center;" type="text" class="fact borde p-1" id="name" name="name" value="{{$user->name}}" />
                                <input style="width:8vw; margin-left:1vw; text-align:center;" type="text" class="fact borde p-1" id="lastname" name="lastname" value="{{$user->lastname}}" />
                                <br>
                                <label style="width:8vw" class="fact">Nombre</label>
                                <label style="width:8vw; margin-left:1vw;" class="fact">Apellidos</label>
                            </div>
                            <div class="form-group mb-0">
                                <input style="width:4vw; text-align:center;" type="numeric" class="fact borde p-1" id="age" name="age" value="{{$user->age}}" />
                                @if($user->type === 3)
                                    <select class="fact borde p-1" style="width:12vw; margin-left:1vw; text-align:center;" name="grade" id="grade" class="form-select">
                                        <option selected>Seleccione una opción...</option>
                                        @foreach($grades as $grade)
                                            @if($grade->id === $role->grade)
                                                <option value="{{ $grade->id }}" selected>{{ $grade->grade_name }}</option>
                                            @elseif($grade->id != $role->id)
                                                <option value="{{ $grade->id }}">{{ $grade->grade_name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                @elseif($user->type === 2)
                                    <select class="fact borde p-1" style="width:12vw; margin-left:1vw; text-align:center;" name="subject" id="subject" class="form-select">
                                        <option selected>Seleccione una opción...</option>
                                        @foreach($subjects as $subject)
                                            @if($subject->id === $role->subject_focus)
                                                <option value="{{ $subject->id }}" selected>{{ $subject->subject }}</option>
                                            @elseif($subject->id != $role->subject_focus)
                                                <option value="{{ $subject->id }}">{{ $subject->subject}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                @endif
                                <br>
                                <label style="width:4vw" class="fact">Edad</label>
                                @if($user->type===3)
                                    <label style="width:12vw; margin-left:1vw;" class="fact">Grado</label>
                                @elseif($user->type ===2 )
                                    <label style="margin-left:1vw; width:12vw" class="fact">Materia</label> 
                                @endif
                            </div>
                            <div class="form-group mb-0">
                                <input style="width:17.3vw; text-align:center;" type="text" class="fact borde p-1" id="institute" name="institute" value="{{$user->institute}}" />
                                <label style="width:17.3vw" class="fact">Centro educativo</label>
                            </div>
                            <h4 class="fact"><strong>Datos de usuario</strong><img class="pl-2" src="https://192.168.1.109/img/icons_pencil.svg" height="23vh" alt=""></h4>
                            <div class="form-group mb-0">
                                <input style="width:17.3vw; text-align:center;" type="text" class="fact borde p-1" id="email" name="email" value="{{$user->email}}" />
                                <label style="width:17.3vw" class="fact">Correo electrónico</label>
                            </div>
                            <div class="form-group mb-0">
                                <input style="width:17.3vw; text-align:center;" type="password" class="fact borde p-1" id="password" name="password" value="userpassword" />
                                <label style="width:17.3vw" class="fact">Contraseña</label>
                            </div>
                            <div class="form-group flex-center">
                            <button type="submit" class="btn blue">APLICAR</button> 
                            </div>
                        </form>
                        <button onclick="document.getElementById('id1').value='{{$user->id}}'; event.preventDefault(); document.getElementById('delete-form').submit();"  class="btn red users" style="margin-left: 6vw">ELIMINAR CUENTA</button>
                        <form id="delete-form" action="{{ route('deleteProfile', $user->id) }}" method="post" style="display: none;">
                            @csrf
                            @method('DELETE')
                            <input id="id1" type="number" name="id1" hidden>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection