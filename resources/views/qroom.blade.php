@extends('layouts.app')


@section('content')
<style>
    html, body{
        font-family: 'sans-serif';
    }
    .blue:hover{
        color:white; 
        background-color: #F1B809;
        text-decoration: none;
        font-family: 'Nunito';
    }

    .blue{
        color:white; 
        background-color: #0D5DA7;
        text-decoration: none;
        font-family: 'Nunito';
    }

    .red:hover{
        color:white; 
        background-color: #F1B809;
        text-decoration: none;
        font-family: 'Nunito';
    }

    .red{
        color:white; 
        background-color: #E62C28EE;
        text-decoration: none;
        font-family: 'Nunito';
    }
    .sala{
        height: 95%;
        width: 19vw;
        border: 2px solid darkgrey;
        border-radius: 10px;
        margin-top: 1.7rem;
    }

    .chat{
        height: 95%;
        width: 50vw;
        border: 2px solid darkgrey;
        border-radius: 10px;
        margin-top: 1.7rem;
    }

    .users{
        margin-left: 0.8rem;
        width: 16vw;
        border: 2px solid lightgrey;
        border-radius: 10px;
        margin-top: 0.5rem;
    }

    .title{
        color:#0D5DA7;
        font-family: 'Nunito';
        font-weight: bold;
        margin-top: 0.5rem;
    }
    .name{
        color:#0D5DA7;
        font-weight: bold;
    }
    .username{
        color:#0D5DA7;
        font-weight: lighter;
    }

    .cont{
        color:#0D5DA7;
        font-weight: lighter;
        border: 2px solid lightgrey;
        border-radius: 7px;
        padding: 0.3rem;
        margin-right:0.5rem;
        white-space:nowrap;
    }

    .flex-center {
        align-items: center;
        display: flex;
        justify-content: center;
    }

    .flex-down-center {
        align-items: flex-end;
        display: flex;
        justify-content: center;
    }

    .scroll::-webkit-scrollbar {
        width: 7px;         
    }

    .scroll::-webkit-scrollbar-thumb {
        background-color: lightgrey;  
        border-radius: 20px;    
        margin-right:0.3rem;     
    }

    .scroll1::-webkit-scrollbar {
        width: 5px;     
    }
    
    .scroll1::-webkit-scrollbar-thumb {
        background-color: lightgrey;  
        width:3px;
        border-radius: 10px;    
        margin-right:0.3rem;     
    }

    .creador{
        display:flex;
        flex-direction:row ;
    }

    .sender{
        border: 2px solid #0D5DA7;
        border-radius: 10px;
    }

    .sms{
        border: 2px solid #0D5DA7;
        border-radius: 30px;
    }

    .div{
        min-width:0.1vw;
        max-width:30vw;
        height:auto;
    }

    .sinborde{
        border:0;
        outline:none;
    }

    .sinborde:focus{
        border:0;
        outline:none;
    }

    .scrollh{
        display:flex; 
        flex-direction:row; 
        flex-wrap:nowrap; 
        width: 15vw; 
    }
</style>

<div class="row" style="width:100%; height:120%">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div><a class="btn blue" style="text-decoration:none; font-weight: bold; font-family:'Nunito';" href="{{ route('questionRoom') }}">VOLVER A LAS SALAS</a> <span style="margin-left:27vw; color: #0D5DA7; font-size:1.9rem;font-family: 'Nunito';">{{$room->name}}</span></div>
        <div class="row scroll" style="display:flex; flex-direction:row; width:80vw; height: 73vh; flex-wrap:nowrap; overflox-x:scroll; overflow-y:hidden">
            <div class="col-md-3 mr-2">
                <div class="sala px-2">
                    <div class="creator py-3 pl-2">
                        <h4><strong>Usurios</strong></h4>
                    </div>
                    <br>
                    <div class="scroll1" style="height: 40%; overflow-y: scroll">
                        <div class="users px-2">
                            <h5><strong>Estudiante</strong></h5>
                            @foreach ($users as $user)
                                @if($room->id_student === $user->id)
                                    <span style="font-family:sans-serif; " class="name">{{ $user->name." ".$user->lastname}}</span>
                                    <br>
                                    <span style="font-family:sans-serif; " class="username">{{ $user->username }}</span>
                                @endif        
                            @endforeach
                        </div>      
                        <div class="users px-2">
                            <h5><strong>Docente</strong></h5>
                            <?php
                                $teacher = 'Todavía no hay docente';
                                $teacher1 = 'Todavía no hay docente';
                                foreach ($users as $user){
                                    if($room->id_teacher === $user->id){
                                        $teacher = $user->name." ".$user->lastname;
                                        $teacher1 = $user->username;
                                    }
                                }
                            ?>
                            <span style="font-family:sans-serif; " class="name">{{ $teacher}}</span>
                            <br>
                            <span style="font-family:sans-serif; " class="username">@if($teacher1 !== 'Todavía no hay docente') {{ $teacher1 }} @elseif($teacher1 === 'Todavía no hay docente') @endif</span>
                        </div>              
                    </div>
                </div>
            </div>
            <div class="chat mr-2 p-3">
                <div style="height:86%">
                    @livewire("liveq-chat", ['room' => $room->id])
                </div>
                @livewire("q-chat-form", ['room' => $room->id])
            </div>
            <div class="sala px-2">
                <br>
                <h2 class="title flex-center" style="font-weight: bold">Material Didáctico</h2>
                <br>
                <div style="height:45vh">
                    <div style="height:100%; overflow-y:auto" class="d-flex flex-column scrollh scroll1">
                        @if (count($materialR)>0)
                            @foreach ($materialR as $mate)
                                    @foreach ($materials as $material)
                                        @if($mate->id_material === $material->id)
                                        <div class="users p-2" style="width:85%">
                                            <table>
                                                <thead>
                                                    <th style="width:70%"></th>
                                                    <th style="width:30%"></th>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td style="width:75%">
                                                            <a class="name" href="{{$material->link}}">{{$material->name}}-{{$material->type}}</a>
                                                        </td>

                                                        @if($usertype === 2)
                                                        <td style="width:25%">
                                                            <button onclick="document.getElementById('id1').value='{{$mate->id}}'; event.preventDefault(); document.getElementById('delete-form').submit();"  class="red users p-2" style="font-size:1rem; width:4vw">Eliminar</button>
                                                            <form id="delete-form" action="{{ route('deleteqRoomMaterial', $mate->id) }}" method="post" style="display: none;">
                                                                @csrf
                                                                @method('DELETE')
                                                                <input id="id1" type="number" name="id" hidden>
                                                            </form>
                                                        </td>
                                                        @endif
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        @endif
                                    @endforeach
                            @endforeach
                        @else
                            <span class="users p-2" style="font-family:sans-serif;color: #0D5DA7; font-size:1rem;">No se encontró material didáctico</span>
                        @endif
                    </div>
                </div>
                @if($usertype === 2)
                    <div class="flex-down-center mt-5"><button class="btn blue" style="font-weight: bold; font-family:'Nunito';" data-toggle="modal" data-target="#createModal">AÑADIR MATERIAL</button></div>
                @endif
            </div>
        </div>
    </div>
</div>
<div style="height:90%" class="modal fade" id="createModal" role="dialog">
    <div style="height:90%" class="modal-dialog">
        <div style="height:height:100%" class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Añadir Material Didáctico</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body container scroll1">
                <table class="table table-striped" style="height:85%; overflow-y:auto">
                    <thead>
                        <th><span>Material Didáctico</span></th>
                        <th><span>Opciones</span></th>
                    </thead>
                    <tbody>
                        @foreach ($materials as $material)
                            @if($material->content_id === $room->content_id || $material->content_id === $room->content1_id || $material->content_id === $room->content2_id)
                                <tr>
                                    <td>
                                        <a style="text-decoration:none" href="{{$material->link}}">{{$material->name}}-{{$material->type}}</a>
                                    </td>
                                    <td>
                                        <button onclick="document.getElementById('idm').value='{{$material->id}}'; document.getElementById('idr').value='{{$room->id}}';  event.preventDefault(); document.getElementById('put-form').submit();"  class="blue users p-2" style="font-size:1rem; width:5vw">Añadir</button>
                                        <form id="put-form" action="{{ route('qroomMaterial') }}" method="post" style="display: none;">
                                            @csrf
                                            <input id="idr" type="number" name="idr" hidden>
                                            <input id="idm" type="number" name="idm" hidden>
                                        </form>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection