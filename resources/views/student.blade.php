@extends('layouts.app')

@section('content')
    @if(isset($list))
    @endif
    
    <style>
    html, body{
        font-family: 'sans-serif';
    }
    textarea, h2, input, th, td, p, span, input, label, option, select, strong, small{
        color:#0D5DA7;

    }
    .title{
        font-family: 'Nunito';
        font-weight: bold;
    }
    .blue:hover{
        color:white; 
        background-color: #F1B809;
        text-decoration: none;
        font-family: 'Nunito';
    }

    .blue{
        color:white; 
        background-color: #0D5DA7;
        text-decoration: none;
        font-family: 'Nunito';
    }

    .red:hover{
        color:white; 
        background-color: #F1B809;
        text-decoration: none;
        font-family: 'Nunito';
    }

    .red{
        color:white; 
        background-color: #E62C28EE;
        text-decoration: none;
        font-family: 'Nunito';
    }

    .scroll::-webkit-scrollbar {
        width: 7px;         
    }

    .scroll::-webkit-scrollbar-thumb {
        background-color: lightgrey;  
        border-radius: 20px;    
        margin-right:0.3rem;     
    }

    .scroll1::-webkit-scrollbar {
        width: 5px;     
    }
    
    .scroll1::-webkit-scrollbar-thumb {
        background-color: lightgrey;  
        width:3px;
        border-radius: 10px;    
        margin-right:0.3rem;     
    }
</style>

<div class="row" style="width:100%; height:100%">
    <div class="col-md-1"></div>
    <div class="col-md-10 row" style="height:75vh;">
        <div class="col-md-4" style="height:73vh;">
            <h2 style="margin-top:5vh"><strong class="title">TAREAS Y PENDIENTES</strong></h2>
            <strong>Frase de motivación</strong>
            <br>
            <p style="width:50%; margin-bottom:0vh;"><i>"{{$frase["frase"]}}"</i></p>
            <strong>{{$frase["autor"]}}</strong>
            <form style="width:58%; margin-top:1vh;" action="{{ route('tostudy') }}"  method="post" accept-charset="UTF-8">
                @csrf
                <div class="form-group column">
                    <label for="" class="col-form-label"><span style="width:100%"><strong>AÑADIR ESTUDIO PENDIENTE</strong></span>
                        <textarea style="margin-top:0.5vh; width:15vw; height: 7vh; border-radius: 8px; border: solid 4px #0D5DA7; padding: 0.5vw; color: gray" name="task" id="task" cols="10" rows="5" placeholder="Estudiar...."></textarea>
                    </label>
                </div>
                <div class="form-group d-flex justify-content-end">
                    <input type="submit" class="btn blue" name="submit" value="AGREGAR A LA LISTA">
                </div>
            </form>
        </div>
        <div class="scroll col-md-8 " style="height:61vh; margin-top:12vh; overflow-y:auto; display:flex; flex-direction:column; align-items:center">
            @if(isset($list))
                @foreach ($list as $lis)
                    <div style="height:5vh; width:75%; border: 3px solid darkgrey; border-radius: 7px; margin-bottom:3vh; padding:0.5vw; display:flex">
                        <div style="width:95%;">
                            <h5 style="width:100%"><strong>{{$lis}}</strong></h5>
                        </div>
                        <div style="width:10%;">
                            <a href="{{ route('tostudydelete', $lis) }}"><img src="https://192.168.1.109/img/check.svg" alt=""></a>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</div>
@endsection