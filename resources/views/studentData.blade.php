@extends('layouts.app')

@section('content')
<style>
    h3, p, span, input, label, option, select{
        color:#0D5DA7;

    }

    .blue:hover{
        color:white; 
        background-color: #F1B809;
        text-decoration: none;
        font-family: 'Nunito';
    }

    .blue{
        color:white; 
        background-color: #0D5DA7;
        text-decoration: none;
        font-family: 'Nunito';
    }

</style>
<br>
<br>
<div style="padding-left:10vw">
    <h3><strong>Registro de Estudiante</strong></h3>
    <br>
    <br>
    <span>Aún queda información que registrar !</span>
    <br>
    <span>Ingresa el nivel en que te encuentras en la</span>
    <br>
    <span>siguiente opción</span>
    <br>
    <br>
    <br>
    <form action="{{ route('createStudent') }}" method="post">
        @csrf
        <div class="form-group">
            <label for="" class="col-form-label"><strong style="padding-right:2vw">Nivel </strong>
                <select name="grade" id="grade" class="form-select" style="border-radius:5px">
                    <option value="@" selected>Seleccione una opción</option>
                        @foreach ($grades as $grade)
                            <option value="{{ $grade->id }}">{{ $grade->grade_number }}</option>
                        @endforeach
                </select>
            </label>
        </div>
        <br>
        <br>
        <div class="form-group ">
            <input type="submit" class="btn blue" name="submit" value="GUARDAR">
        </div>
    </form>   
</div>

@endsection