@extends('layouts.app')

@section('content')
<style>
    html, body{
        font-family: 'sans-serif';
    }
    .blue:hover{
        color:white; 
        background-color: #F1B809;
        text-decoration: none;
        font-family: 'Nunito';
    }

    .blue{
        color:white; 
        background-color: #0D5DA7;
        text-decoration: none;
        font-family: 'Nunito';
    }

    .red:hover{
        color:white; 
        background-color: #F1B809;
        text-decoration: none;
        font-family: 'Nunito';
    }

    .red{
        color:white; 
        background-color: #E62C28EE;
        text-decoration: none;
        font-family: 'Nunito';
    }
    .sala{
        height: 95%;
        width: 20vw;
        border: 2px solid darkgrey;
        border-radius: 10px;
        margin-top: 1.7rem;
    }
    .users{
        margin-left: 0.8rem;
        width: 16vw;
        border: 2px solid lightgrey;
        border-radius: 10px;
        margin-top: 0.5rem;
    }
    .title{
        color:#0D5DA7;
        font-family: 'Nunito';
        font-weight: bold;
        margin-top: 0.5rem;
    }
    .name{
        color:#0D5DA7;
        font-weight: bold;
    }
    .username{
        color:#0D5DA7;
        font-weight: lighter;
    }

    .cont{
        color:#0D5DA7;
        font-weight: lighter;
        border: 2px solid lightgrey;
        border-radius: 7px;
        padding: 0.3rem;
        margin-right:0.5rem;
        white-space:nowrap;
    }

    .flex-center {
        align-items: center;
        display: flex;
        justify-content: center;
    }
    .scroll::-webkit-scrollbar {
        width: 7px;         
    }
    .scroll::-webkit-scrollbar-thumb {
        background-color: lightgrey;  
        border-radius: 20px;    
        margin-right:0.3rem;     
    }
</style>

<div class="row" style="width:100%; height:120%">
    <div class="col-md-1"></div>
    <div class="col-md-11">
        <div> <button class="btn blue" style="font-weight: bold; font-family:'Nunito';" data-toggle="modal" data-target="#createModal" onclick="return cleanfields();">CREAR SALA DE ESTUDIO +</button></div>
        <div class="row scroll" style="display:flex; flex-direction:row; width:80vw; height: 73vh; flex-wrap:nowrap; overflox-x:scroll; overflow-y:hidden">
            @foreach ($rooms as $room)
                @if($auth === $room->creator)
                    <?php
                        $materia;
                        $contenido1;
                        $contenido2;
                        $contenido3;
                        $invitados='';
                        foreach ($subjects as $subject){
                            if ($subject->id === $room->subject_id) {
                                $materia=$subject->subject;
                            }
                        }
                        foreach ($contents as $content){
                            if ($content->id === $room->content_id) {
                                $contenido1=$content->content;
                            }
                        }
                        foreach ($contents as $content){
                            if ($content->id === $room->content1_id) {
                                $contenido2=$content->content;
                            }
                        }
                        foreach ($contents as $content){
                            if ($content->id === $room->content2_id) {
                                $contenido3=$content->content;
                            }
                        }
                    ?>
                    <div class="col-md-3 mr-5">
                        <div class="sala px-2">
                            <div class="flex-center"><h2 class="title">{{ $room->name }}</h2></div>
                            <div class="mx-3">
                                <h5 class="name">Materia:</h5>
                                <span class="cont">{{ $materia }}</span>
                                <span class="cont">{{ $room->date_room }}/{{ $room->time_room }}</span> 
                                <h5 style="margin-top:0.5rem" class="name">Contenidos:</h5>
                                <div class="scroll" style="display:flex; flex-direction:row; flex-wrap:nowrap; padding-top:0.5rem; padding-bottom:0.5rem; width: 15vw; height: 7vh; overflow-x: scroll; overflow-y:hidden">
                                    <span class="cont">{{ $contenido1 }}</span>
                                    <span class="cont">{{ $contenido2 }}</span>
                                    <span class="cont">{{ $contenido3 }}</span>
                                </div>
                                <h5 style="margin-top:0.5rem" class="name">Usuarios</h5>
                            </div>
                            <div class="scroll1" style="height: 40%; overflow-y: scroll">
                                @foreach ($users_rooms as $user_room)
                                    @if($user_room->id_room === $room->id)
                                        @foreach ($users as $user)
                                            @if($user->id === $user_room->id_user)
                                                <div class="users px-2">
                                                    <span class="name">{{ $user->name." ".$user->lastname}}</span>
                                                    <br>
                                                    <span class="username">{{ $user->username }}</span>
                                                </div>
                                                <?php 
                                                    $tmp = $invitados;
                                                    $invitados = $tmp.$user->username.','
                                                ?>
                                            @endif
                                        @endforeach
                                    @endif
                                @endforeach
                            </div>
                            <div class="flex-center mb-2" style="margin:0px">
                                <a class="btn blue" style="width: 14.3vw;; text-decoration:none; color:white" href="{{ route('sroom', $room->id) }}">INICIAR</a>
                            </div>
                            <div class="flex-center">
                                <button class="btn blue" style="width:7vw; " type="button" class="btn blue" data-toggle="modal" data-target="#createModal" onclick="document.getElementById('id').value='{{$room->id}}'; document.getElementById('name').value='{{$room->name}}'; document.getElementById('subject').value = {{$room->subject_id}}; document.getElementById('cont1').value='{{$room->content_id}}'; document.getElementById('cont2').value='{{$room->content1_id}}'; document.getElementById('cont3').value='{{$room->content2_id}}'; document.getElementById('users').value='{{$invitados}}';document.getElementById('date').value='{{$room->date_room}}';document.getElementById('time').value='{{$room->time_room}}';">Editar</button>
                                <button style="width:7vw; " class="ml-2 btn red" onclick="document.getElementById('id1').value='{{ $room->id}}';  event.preventDefault(); document.getElementById('delete-form').submit();" class="btn red" type="button">Delete</button>
                                <form id="delete-form" action="{{ route('deleteStudyRoom', $room->id) }}" method="post" style="display: none;">
                                    @csrf
                                    @method('DELETE')
                                    <input id="id1" type="number" name="id1" hidden>
                                </form>
                            </div>
                        </div>
                    </div>
                @endif
            @endforeach
            @foreach($rooms as $room)
                @foreach ($users_rooms as $u_room)
                    @if($u_room->id_room === $room->id && $auth !== $room->creator && $u_room->id_user === $auth)
                        <?php
                            $materia;
                            $contenido1;
                            $contenido2;
                            $contenido3;
                            $invitados='';
                            foreach ($subjects as $subject){
                                if ($subject->id === $room->subject_id) {
                                    $materia=$subject->subject;
                                }
                            }
                            foreach ($contents as $content){
                                if ($content->id === $room->content_id) {
                                    $contenido1=$content->content;
                                }
                            }
                            foreach ($contents as $content){
                                if ($content->id === $room->content1_id) {
                                    $contenido2=$content->content;
                                }
                            }
                            foreach ($contents as $content){
                                if ($content->id === $room->content2_id) {
                                    $contenido3=$content->content;
                                }
                            }
                        ?>
                        <div class="col-md-3 mr-5">
                            <div class="sala px-2">
                                <div class="flex-center"><h2 class="title">{{ $room->name }}</h2></div>
                                <div class="mx-3">
                                    <span style="color:#0D5DA7;font-weight: lighter;">Invitado</span>
                                    <h5 class="name">Materia:</h5>
                                    <span class="cont">{{ $materia }}</span> 
                                    <span class="cont">{{ $room->date_room }}/{{ $room->time_room }}</span> 
                                    <h5 style="margin-top:0.5rem" class="name">Contenidos:</h5>
                                    <div class="scroll" style="display:flex; flex-direction:row; flex-wrap:nowrap; padding-top:0.5rem; padding-bottom:0.5rem; width: 15vw; height: 7vh; overflow-x: scroll; overflow-y:hidden">
                                        <span class="cont">{{ $contenido1 }}</span>
                                        <span class="cont">{{ $contenido2 }}</span>
                                        <span class="cont">{{ $contenido3 }}</span>
                                    </div>
                                    <h5 style="margin-top:0.5rem" class="name">Usuarios</h5>
                                </div>
                                <div class="scroll1" style="height: 40%; overflow-y: scroll">
                                    @foreach ($users_rooms as $user_room)
                                        @if($user_room->id_room === $room->id)
                                            @foreach ($users as $user)
                                                @if($user->id === $user_room->id_user)
                                                    <div class="users px-2">
                                                        <span class="name">{{ $user->name." ".$user->lastname}}</span>
                                                        <br>
                                                        <span class="username">{{ $user->username }}</span>
                                                    </div>
                                                    <?php 
                                                        $tmp = $invitados;
                                                        $invitados = $tmp.$user->username.','
                                                    ?>
                                                @endif
                                            @endforeach
                                        @endif
                                    @endforeach
                                </div>
                                <div class="flex-center mb-2" style="margin:0px">
                                    <a class="btn blue" style="width: 14.3vw; text-decoration:none; color:white" href="{{ route('sroom', $room->id) }}">INICIAR</a>
                                </div>
                                <div class="flex-center">
                                    <button style="width:7vw; " class="ml-2 btn red" onclick="document.getElementById('id1').value='{{ $room->id}}';  event.preventDefault(); document.getElementById('delete-form').submit();" class="btn red" type="button">Salir de la sala</button>
                                    <form id="delete-form" action="{{ route('exitStudyRoom', $room->id) }}" method="post" style="display: none;">
                                        @csrf
                                        @method('PUT')
                                        <input id="id1" type="number" name="id1" hidden>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            @endforeach
        </div>
        <div class="modal fade" id="createModal" role="dialog">
            <?php
                $sub="";
            ?>
            <div class="modal-dialog">
            
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Crear Sala</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body container">
                        <form style="heigth:40vh; width:100%" action="{{ route('createStudyRoom') }}" method="post" accept-charset="UTF-8" enctype="multipart/form-data" onsubmit=" document.getElementById('user').disabled = false;">
                            @csrf

                            <div class="form-group row">
                                <input id="id" type="number" name="id" hidden>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-md-9 col-form-label"><span>Nombre de la sala:</span>
                                    <input type="text" id="name" name="name">
                                </label>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-md-9 col-form-label"><span>Materia:</span><select name="subject" id="subject" class="form-select" onchange="return showfields();">
                                    <option value="@" selected>Seleccione una opción...</option>
                                    @foreach ($subjects as $subject)
                                        <option value="{{ $subject->id }}">{{ $subject->subject }}</option>
                                    @endforeach
                                </select></label>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-md-9 col-form-label"><span>Contenido 1:</span>
                                    <select name="cont1" id="cont1" class="form-select">
                                        <option value="@" selected>Seleccione una opción...</option>
                                        @foreach ($contents as $content)
                                            <option value="{{ $content->id }}">{{ $content->content }}</option>
                                        @endforeach
                                    </select>
                                </label>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-md-9 col-form-label"><span>Contenido 2:</span>
                                    <select name="cont2" id="cont2" class="form-select">
                                        <option value="@" selected>Seleccione una opción...</option>
                                        @foreach ($contents as $content)
                                            <option value="{{ $content->id }}">{{ $content->content }}</option>
                                        @endforeach
                                    </select>
                                </label>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-md-9 col-form-label"><span>Contenido 3:</span>
                                <select name="cont3" id="cont3" class="form-select">
                                        <option value="@" selected>Seleccione una opción...</option>
                                        @foreach ($contents as $content)
                                            <option value="{{ $content->id }}">{{ $content->content }}</option>
                                        @endforeach
                                    </select>
                                </label>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-md-9 col-form-label"><span>Usuarios:</span>
                                    <input type="text" id="users" name="users">
                                </label>
                                <p style="padding-left: 0.9rem; font-size:0.7rem; color: gray">Ingrese los nombres de usuario, sin espacio y con comas. Solamente se ingresan 10 usuarios</p>
                            </div>
                            
                            <div class="form-group row">
                                <label for="" class="col-md-9 col-form-label"><span>Fecha de uso:</span>
                                    <input type="date" name="date" id="date">
                                </label>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-md-9 col-form-label"><span>Hora de uso:</span>
                                    <input type="time" name="time" id="time">
                                </label>
                            </div>

                            <div class="form-group text-center">
                                <input type="submit" class="btn blue" name="submit" value="Guardar">
                            </div>

                        </form>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function cleanfields(){
        document.getElementById('id').value='';
        document.getElementById('name').value='';
        document.getElementById('subject').value='@';
        document.getElementById('cont1').value='@';
        document.getElementById('cont2').value='@';
        document.getElementById('cont3').value='@';
        document.getElementById('users').value='';
        document.getElementById('date').value='';
        document.getElementById('time').value='';
    }
</script>
@endsection