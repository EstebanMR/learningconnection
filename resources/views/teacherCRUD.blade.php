@extends('layouts.app')

@section('content')

<style>
    h2, h4, input, th, td, p, span, input, label, option, select{
        color:#0D5DA7;

    }
    
    .blue:hover{
        color:white; 
        background-color: #F1B809;
        text-decoration: none;
        font-family: 'Nunito';
    }

    .blue{
        color:white; 
        background-color: #0D5DA7;
        text-decoration: none;
        font-family: 'Nunito';
    }

    .red:hover{
        color:white; 
        background-color: #F1B809;
        text-decoration: none;
        font-family: 'Nunito';
    }

    .red{
        color:white; 
        background-color: #E62C28EE;
        text-decoration: none;
        font-family: 'Nunito';
    }
</style>

<div class="container" style="height: 73vh;">
    <div class="row justify-content-center" style="height:100%;">
        <div class="col-md-8" style="height:100%;">
            <div class="card" style="height:100%;">

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    
                    <h2>Profesores</h2>
                    <button type="button" class="btn blue" data-toggle="modal" data-target="#createModal" onclick="return cleanfields();">Crear Profesor</button>

                    <br>
                    <br>
                    <div class=" scroll1 table-responsive" style="height:85%; overflow-y:auto">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Usuario</th>
                                    <th scope="col">Documentos</th>
                                    <th scope="col">Correo</th>
                                    <th scope="col">Tipo</th>
                                    <th scope="col">Ciclo</th>
                                    <th scope="col">Materia</th>
                                    <th scope="col">Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $index=0;
                                ?>
                                @foreach($teachers as $teacher)
                                    <?php
                                        $index++;
                                        $userName;
                                        $userMail;
                                        foreach ($users as $user){
                                            if ($user->id === $teacher->user_id) {
                                                $userName = $user->username;
                                                $userMail = $user->email;
                                            }
                                        }
                                    ?>
                                    <?php
                                        $subjectName;    
                                        foreach ($subjects as $subject){
                                            if (intval($teacher->subject_focus) === $subject->id) {
                                                $subjectName = $subject->subject;
                                            }
                                        }
                                    ?>
                                    <tr>
                                        <td>{{ $index }}</td>
                                        <td>{{ $userName }}</td>
                                        <td>
                                            <?php 
                                            $thefolder = Storage::files("public/".$teacher->user_id);
                                            if (count($thefolder) >=1) {
                                                foreach ($thefolder as $file) {
                                                    $temp = explode("/", $file);
                                                    echo '<a href="'.Storage::url($file).'">'.$temp[2].'</a><br>';
                                                }
                                            }
                                            ?>
                                        <td>{{ $userMail }}</a></td>
                                        <td>{{ $teacher->institute_focus }}</td>
                                        <td>{{ $teacher->cicle_focus }}</td>
                                        <td>{{ $subjectName ?? '' }}</td>
                                        <td>@if($teacher->active === 1)
                                            <button type="button" class="btn blue" data-toggle="modal" data-target="#createModal" onclick="document.getElementById('id').value='{{$teacher->id}}'; document.getElementById('user').value='{{$teacher->user_id}}'; document.getElementById('user').disabled = true; document.getElementById('institute').value='{{$teacher->institute_focus}}'; document.getElementById('subject').value='{{$teacher->subject_focus}}'; document.getElementById('cicle').value='{{$teacher->cicle_focus}}'; return showfields();">Editar</button>
                                            <button onclick="document.getElementById('id1').value='{{$teacher->id}}';  event.preventDefault(); document.getElementById('delete-form').submit();" class="btn red" type="button">Delete</button>
                                            <form id="delete-form" action="{{ route('deleteTeacher', $teacher->id) }}" method="post" style="display: none;">
                                                @csrf
                                                @method('DELETE')
                                                <input id="id1" type="number" name="id1" hidden>
                                            </form>
                                            @else 
                                                <button onclick="document.getElementById('idt').value='{{$teacher->id}}';  event.preventDefault(); document.getElementById('accept-form').submit();" class="btn blue" type="button">Aceptar</button>
                                                <form id="accept-form" action="{{ route('acceptTeacher', $teacher->id) }}" method="post" style="display: none;">
                                                    @csrf
                                                    <input id="idt" type="number" name="idt" hidden>
                                                </form>
                                                <button onclick="document.getElementById('id1').value='{{$teacher->id}}';  event.preventDefault(); document.getElementById('delete-form').submit();" class="btn red" type="button">Delete</button>
                                                <form id="delete-form" action="{{ route('deleteTeacher', $teacher->id) }}" method="post" style="display: none;">
                                                    @csrf
                                                    @method('DELETE')
                                                    <input id="id1" type="number" name="id1" hidden>
                                                </form>
                                            @endif</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="createModal" role="dialog">
        <div class="modal-dialog">
        
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Crear Profesor</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <form style="heigth:40vh; width:100%" action="{{ route('createTeacher') }}" method="post" accept-charset="UTF-8" enctype="multipart/form-data" onsubmit=" document.getElementById('user').disabled = false;">
                            @csrf

                            <div class="form-group row">
                                <input id="id" type="number" name="id" hidden>
                            </div>

                            <div class="form-group row">
                                <label for="" class=" col-form-label"><span>Usuario:</span><select name="user" id="user" class="form-select">
                                    <option value="@" selected>Seleccione una opción...</option>
                                    @foreach ($users as $user)
                                        <option value="{{ $user->id }}">{{ $user->username }}</option>
                                    @endforeach
                                </select></label>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-form-label"><span>Tipo:</span><select name="institute" id="institute" class="form-select" onchange="return showfields();">
                                    <option value="@" selected>Seleccione una opción...</option>
                                    <option value="Escuela">Escuela</option>
                                    <option value="Colegio">Colegio</option>
                                </select></label>
                            </div>

                            <div class="form-group row">
                                <label for="" class="ccol-form-label"><span>Ciclo:</span><select name="cicle" id="cicle" class="form-select">
                                    <option value="@" selected>Seleccione una opción...</option>
                                    <option id="ciclos esc" value="Primer y segundo">Primer y segundo</option>
                                    <option id="ciclos col" value="Tercer y cuarto">Tercer y cuarto</option>
                                </select></label>
                            </div>
                            
                            <div class="form-group row">
                                <label for="" class="col-form-label"><span>Materia:</span><select name="subject" id="subject" class="form-select">
                                    <option value="@" selected>Seleccione una opción...</option>
                                    @foreach ($subjects as $subject)
                                        <option value="{{ $subject->id }}">{{ $subject->subject }}</option>
                                    @endforeach
                                </select></label>
                            </div>

                            <label>Por favor ingrese sus acreditaciones de maestro</label>

                            <div id = "local" class="form-group row">
                                <label for="" class="col-form-label"><span>Archivos:</span><input id="docs[]" type="file" name="docs[]" multiple></label>
                            </div>

                            <div class="form-group text-center">
                                <input type="submit" class="btn blue" name="submit" value="Guardar">
                            </div>

                        </form>  
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function showfields(){
        var dropdown = document.getElementById('institute');
        var opt = dropdown.options [dropdown.selectedIndex].value;
        if (opt == "Escuela") {
            document.getElementById('ciclos esc').style.visibility = 'visible';
            document.getElementById('ciclos col').style.visibility = 'hidden';
            document.getElementById('ciclos esc').disabled=false;
            document.getElementById('ciclos col').disabled=true;
        } else {
            document.getElementById('ciclos esc').style.visibility = 'hidden';
            document.getElementById('ciclos col').style.visibility = 'visible';
            document.getElementById('ciclos esc').disabled=true;
            document.getElementById('ciclos col').disabled=false;
        }
    }
    function cleanfields(){
        document.getElementById('id').value='';
        document.getElementById('user').value='@';
        document.getElementById('institute').value='@';
        document.getElementById('cicle').value='@';
        document.getElementById('subject').value='@';
    }
</script>

@endsection