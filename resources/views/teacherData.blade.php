<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Learning Connection</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://js.pusher.com/4.0/pusher.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.min.js" integrity="sha384-VHvPCCyXqtD5DqJeNxl2dtTyhF78xXNXdkwX1CZeRusQfRKp+tA7hAShOK/B/fQ2" crossorigin="anonymous"></script>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <style>
            .btn{
            font-family: 'Nunito';
            font-size: 1rem
        }
        .flex-center {
            display: flex;
            justify-content: center;
        }
        span, p{
            font-family: "Open Sans";
        }
        .ref{
            color:#0D5DA7; 
            font-family: 'Nunito';
            padding: 0.7rem;
            font-weight: bold;
            font-size: 1.1rem;
            border-radius: 7px
        }

        .ref:hover{
            color:#F1B809; 
            text-decoration: none;
            font-family: 'Nunito';
            padding: 0.7rem;
            font-weight: bold;
            font-size: 1.1rem;
        }

        .active{
            color:#E62C28; 
            text-decoration: none;
            font-family: 'Nunito';
            padding: 0.7rem;
            font-weight: bold;
            font-size: 1.1rem;
        }

        .active:hover{
            color:#F1B809; 
            text-decoration: none;
            font-family: 'Nunito';
            padding: 0.7rem;
            font-weight: bold;
            font-size: 1.1rem;
        }

        .active{
            color:#E62C28; 
            text-decoration: none;
            font-family: 'Nunito';
            padding: 0.7rem;
            font-weight: bold;
            font-size: 1.1rem;
        }

        .active:hover{
            color:#F1B809; 
            text-decoration: none;
            font-family: 'Nunito';
            padding: 0.7rem;
            font-weight: bold;
            font-size: 1.1rem;
        }

        .inicio{
            padding-top: 0.7rem;
            padding-left: 0.5rem;
        }

        .inicio:hover{
            color:#F1B809;
            padding-top: 0.7rem;
            padding-left: 0.5rem;
        }

        .img:hover{
            content:url(https://192.168.1.109/img/inicio1.svg);
        }
        .scroll1::-webkit-scrollbar {
        width: 5px;     
        }
        .scroll1::-webkit-scrollbar-thumb {
            background-color: lightgrey;  
            width:3px;
            border-radius: 10px;    
            margin-right:0.3rem;     
        }
            h3, p, span, input, label, option, select{
                color:#0D5DA7;

            }

            .blue:hover{
                color:white; 
                background-color: #F1B809;
                text-decoration: none;
                font-family: 'Nunito';
            }

            .blue{
                color:white; 
                background-color: #0D5DA7;
                text-decoration: none;
                font-family: 'Nunito';
            }
            .inputfile::before{
                border: 2px solid darkgrey;
                border-radius:7px;
                color: #0D5DA7;
            }

        </style>
    </head>
    <body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <a style="margin-left:3.3vw" href="{{ route('home') }}"><img src="https://192.168.1.109/img/Isotipo.svg" height="45vh"><img src="https://192.168.1.109/img/letras.svg" height="25vh" style="padding-left:0.5rem"></a>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav mr-5">
                        <!-- Authentication Links -->
                        <span class="ref">CONSULTAS</span>
                        <span class="ref">SOLICITUDES</span>
                        <span class="ref">MATERIAL DIDÁCTICO</span>
                        <span class="ref">PERFIL</span>
                            
                        <a class="inicio" href="{{route('logout')}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <span> @yield('user')</span>
                            <img class="img" src="https://192.168.1.109/img/inicio.svg" height="17vh">
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </ul>
                </div>
            </div>
        </nav>
        <div style=" height: 4vh; weight:100%; background: -prefix-linear-gradient(top, drarkgrey, white); background: linear-gradient(to bottom, darkgrey, white);"></div>
            <main>
                @include('flash-message')
                <br>
                <br>
                <div style="padding-left:10vw">
                    <h3><strong>Registro de Docente</strong></h3>
                    <br>
                    <br>
                    <span>Aún queda información que registrar !</span>
                    <br>
                    <span>Ingrese el tipo de institución, el ciclo y la materia que se especializa en las</span>
                    <br>
                    <span>siguientes opciónes</span>
                    <br>
                    <br>
                    <br>
                    <form style="heigth:40vh; width:80%" action="{{ route('applyTeacher') }}" method="post" accept-charset="UTF-8" enctype="multipart/form-data" onsubmit=" document.getElementById('user').disabled = false;">
                        @csrf
                        <div class="form-group row" style="padding-left:1vw">
                            <label for="" class="col-form-label"><strong style="padding-right:3.3vw">Tipo</strong><select name="institute" id="institute" class="form-select" onchange="return showfields();">
                                <option value="@" selected>Seleccione una opción...</option>
                                <option value="Escuela">Escuela</option>
                                <option value="Colegio">Colegio</option>
                            </select></label>
                        </div>

                        <div class="form-group row" style="padding-left:1vw">
                            <label for="" class="ccol-form-label"><strong style="padding-right:3.1vw">Ciclo</strong><select name="cicle" id="cicle" class="form-select">
                                <option value="@" selected>Seleccione una opción...</option>
                                <option id="ciclos esc" value="Primer y segundo">Primer y segundo</option>
                                <option id="ciclos col" value="Tercer y cuarto">Tercer y cuarto</option>
                            </select></label>
                        </div>
                                            
                        <div class="form-group row" style="padding-left:1vw">
                            <label for="" class="col-form-label"><strong style="padding-right:2.1vw">Materia</strong><select name="subject" id="subject" class="form-select">
                                <option value="@" selected>Seleccione una opción...</option>
                                @foreach ($subjects as $subject)
                                    <option value="{{ $subject->id }}">{{ $subject->subject }}</option>
                                @endforeach
                            </select></label>
                        </div>

                        <label>Por favor ingrese sus acreditaciones de maestro</label>

                        <div id = "local" class="form-group">
                            <label for="" class="col-form-label"><strong style="padding-right:2vw">Archivos</strong></label>
                            <br>
                            <input class="inputfile" id="docs[]" type="file" name="docs[]" multiple>
                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn blue" name="submit" value="Guardar">
                        </div>

                    </form>
                </div>
            </main>
        </div>
        <footer style="width:100%; height:9vh; background-color: #0D5DA7; position:absolute; bottom:0px; padding: 1.3vh; padding-left:4vw;">
            <img src="https://192.168.1.109/img/logoB.svg" height="60vh"><img src="https://192.168.1.109/img/letrasB.svg" height="40vh" style="padding-left:0.5rem">
        </footer>
    </body>
</html>
