@extends('layouts.app')

@section('content')
<style>
    html, body{
        font-family: 'sans-serif';
    }
    h2, input, th, td, p, span, input, label, option, select, strong, small{
        color:#0D5DA7;

    }
    .title{
        font-family: 'Nunito';
        font-weight: bold;
    }
    .blue:hover{
        color:white; 
        background-color: #F1B809;
        text-decoration: none;
        font-family: 'Nunito';
    }

    .blue{
        color:white; 
        background-color: #0D5DA7;
        text-decoration: none;
        font-family: 'Nunito';
    }

    .red:hover{
        color:white; 
        background-color: #F1B809;
        text-decoration: none;
        font-family: 'Nunito';
    }

    .red{
        color:white; 
        background-color: #E62C28EE;
        text-decoration: none;
        font-family: 'Nunito';
    }

    .scroll::-webkit-scrollbar {
        width: 7px;         
    }

    .scroll::-webkit-scrollbar-thumb {
        background-color: lightgrey;  
        border-radius: 20px;    
        margin-right:0.3rem;     
    }

    .scroll1::-webkit-scrollbar {
        width: 5px;     
    }
    
    .scroll1::-webkit-scrollbar-thumb {
        background-color: lightgrey;  
        width:3px;
        border-radius: 10px;    
        margin-right:0.3rem;     
    }
</style>

<div class="row" style="width:100%; height:100%">
    <div class="col-md-1"></div>
    <div class="col-md-10" style="height:75vh;">
        <div style="height:5%; margin-top:5vh;"><h2><strong class="title">SALAS DE CONSULTA DOCENTES</strong></h2></div>
        <br>
        <div style="height:90%;">
            @foreach ($myrooms as $myroom)
                <?php
                    $sub;
                    $cont;
                    $teacher;
                    foreach ($subjects as $subject){
                        if ($myroom->subject_id === $subject->id) {
                            $sub = $subject->subject;
                        }
                    }
                    foreach ($contents as $content){
                        if ($myroom->content_id === $content->id) {
                            $cont = $content->content;
                        }
                    }
                    foreach ($users as $user){
                        if ($myroom->id_teacher === $user->id) {
                            $teacher = $user->username;
                        }
                    }
                    if (!isset($teacher)) {
                        $teacher = "Todavía no hay docente";
                    }
                ?>
                <h4><strong>SALAS DEL DOCENTE</strong></h4>
                <div class="row">
                    <div style="height:10vh; width:80%; border: 3px solid darkgrey; border-radius: 7px; margin-bottom:3vh; padding:1vw; display:flex">
                        <div style="width:85%;">
                            <h5 style="width:100%"><strong>{{$myroom->content}}</strong></h5>
                            <span>Asignatura: {{$sub}}/ Contenido: {{$cont}} / Docente: {{$teacher}}</span>
                        </div>
                        <div style="width:30%; margin-top: 1%">
                            <a href="{{ route('qroom', $myroom->id) }}" style="width:10vw" type="button" class="btn blue">Ingresar</a>
                        </div>
                    </div>
                </div>
            @endforeach
            @foreach ($rooms as $room)
                <?php
                    $sub;
                    $cont;
                    $teacher;
                    foreach ($subjects as $subject){
                        if ($room->subject_id === $subject->id) {
                            $sub = $subject->subject;
                        }
                    }
                    foreach ($contents as $content){
                        if ($room->content_id === $content->id) {
                            $cont = $content->content;
                        }
                    }
                    foreach ($users as $user){
                        if ($room->id_teacher === $user->id) {
                            $teacher = $user->username;
                        }
                    }
                    if (!isset($teacher)) {
                        $teacher = "Todavía no hay docente";
                    }
                ?>
                <h4><strong>SALAS SIN DOCENTE</strong></h4>
                <div class="row">
                    <div style="height:10vh; width:80%; border: 3px solid darkgrey; border-radius: 7px; margin-bottom:3vh; padding:1vw; display:flex">
                        <div style="width:85%;">
                            <h5 style="width:100%"><strong>{{$room->content}}</strong></h5>
                            <span>Asignatura: {{$sub}}/ Contenido: {{$cont}} / Docente: {{$teacher}}</span>
                        </div>
                        <div style="width:30%; margin-top: 1%">
                            <a href="{{ route('acceptqroom', $room->id) }}" style="width:10vw" type="button" class="btn blue">Aceptar</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection