@extends('layouts.app')

@section('content')
<style>
    html, body{
        font-family: 'sans-serif';
    }
    textarea, h2, input, th, td, p, span, input, label, option, select, strong, small{
        color:#0D5DA7;

    }
    .title{
        font-family: 'Nunito';
        font-weight: bold;
    }
    .blue:hover{
        color:white; 
        background-color: #F1B809;
        text-decoration: none;
        font-family: 'Nunito';
    }

    .blue{
        color:white; 
        background-color: #0D5DA7;
        text-decoration: none;
        font-family: 'Nunito';
    }

    .red:hover{
        color:white; 
        background-color: #F1B809;
        text-decoration: none;
        font-family: 'Nunito';
    }

    .red{
        color:white; 
        background-color: #E62C28EE;
        text-decoration: none;
        font-family: 'Nunito';
    }

    .scroll::-webkit-scrollbar {
        width: 7px;         
    }

    .scroll::-webkit-scrollbar-thumb {
        background-color: lightgrey;  
        border-radius: 20px;    
        margin-right:0.3rem;     
    }

    .scroll1::-webkit-scrollbar {
        width: 5px;     
    }
    
    .scroll1::-webkit-scrollbar-thumb {
        background-color: lightgrey;  
        width:3px;
        border-radius: 10px;    
        margin-right:0.3rem;     
    }
</style>

<div class="row" style="width:100%; height:100%">
    <div class="col-md-1"></div>
    <div class="col-md-10 row" style="height:75vh;">
        <div class="col-md-4" style="height:73vh;">
            <h2 style="margin-top:5vh"><strong class="title">SALAS DE CONSULTA</strong></h2>
            <br>
            <div class="col-md-11" style="height:45vh; border: 3px solid darkgrey; border-radius: 5px; padding-left:3vw; padding-top:1.5vw;">
                <form action="{{ route('cquestionRoom') }}"  method="post" accept-charset="UTF-8">
                    @csrf
                    <div class="form-group row">
                        <label for="" class="col-form-label"><span><strong>Materia</strong></span><select style="margin-left:3.2vw; width:8.7vw;" name="subject" id="subject" class="form-select">
                            <option value="@" selected>Seleccione una opción...</option>
                                @foreach ($subjects as $subject)
                                    <option value="{{ $subject->id }}">{{ $subject->subject }}</option>
                                @endforeach
                        </select></label>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-form-label"><span><strong>Contenido</strong></span><select style="margin-left:2.4vw; width:8.7vw;" name="content" id="content" class="form-select">
                            <option value="@" selected>Seleccione una opción...</option>
                                @foreach ($contents as $content)
                                    <option value="{{ $content->id }}">{{ $content->content }}</option>
                                @endforeach
                        </select></label>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-form-label"><span><strong>Consulta</strong></span><br>
                            <textarea style="margin-left:5.3vw; margin-top:-1vw;width:8.7vw;" name="question" id="question" cols="30" rows="10" placeholder="Ingrese su consulta"></textarea>
                        </label>
                    </div>
                    <div class="form-group d-flex justify-content-center">
                        <input style="width: 8vw" type="submit" class="btn blue" name="submit" value="Realizar consulta">
                    </div>
                </form>
            </div>
        </div>
        <div class="scroll col-md-8 " style="height:61vh; margin-top:12vh; overflow-y:auto; display:flex; flex-direction:column; align-items:center">
            @foreach ($rooms as $room)
                <?php
                    $sub;
                    $cont;
                    $teacher;
                    foreach ($subjects as $subject){
                        if ($room->subject_id === $subject->id) {
                            $sub = $subject->subject;
                        }
                    }
                    foreach ($contents as $content){
                        if ($room->content_id === $content->id) {
                            $cont = $content->content;
                        }
                    }
                    foreach ($users as $user){
                        if ($room->id_teacher === $user->id) {
                            $teacher = $user->username;
                        }
                    }
                    if (!isset($teacher)) {
                        $teacher = "Todavía no hay docente";
                    }
                ?>
                <div style="height:10vh; width:80%; border: 3px solid darkgrey; border-radius: 7px; margin-bottom:3vh; padding:1vw; display:flex">
                    <div style="width:85%;">
                        <h5 style="width:100%"><strong>{{$room->content}}</strong></h5>
                        <span>Asignatura: {{$sub}}/ Contenido: {{$cont}} / Docente: {{$teacher}}</span>
                    </div>
                    <div style="width:40%; margin-top: 2%">
                        <a href="{{ route('qroom', $room->id) }}" style="width:6vw" type="button" class="btn blue">Iniciar</a>
                        <button style="width:6vw" onclick="document.getElementById('id1').value='{{$room->id}}';  event.preventDefault(); document.getElementById('delete-form').submit();" class="btn red" type="button">Delete</button>
                        <form id="delete-form" action="{{ route('deleteQRoom', $room->id) }}" method="post" style="display: none;">
                            @csrf
                            @method('DELETE')
                            <input id="id1" type="number" name="id1" hidden>
                        </form>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection