<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

        <title>Laravel</title>

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: sans-serif;
                font-weight: 10;
                height: 70vh;
                margin: 0;
            }

            .blue:hover{
                color:white; 
                background-color: #F1B809;
                text-decoration: none;
                font-family: 'Nunito';
            }

            .blue{
                color:white; 
                background-color: #0D5DA7;
                text-decoration: none;
                font-family: 'Nunito';
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-weight: 900;
                font-size: 1.3rem; 
                color:#0D5DA7
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            input, button, h1, h2, h3{
                font-family: 'Nunito';
                margin:0px
            }

            .abs-center {
                display:flex;
                align-items: center;
                justify-content: center;
                min-height: 40vh
            }
        </style>
    </head>
    <body>
        <div>
            @if (Route::has('login'))
                <div class="px-1">
                    <div class="position-absolute top-100 start-100 translate-middle" style="margin-top:1%; padding-left: 2%; width:15vw">
                    <a href="{{ route('home') }}"><img src="https://192.168.1.109/img/Isotipo.svg" height="45vh"><img src="https://192.168.1.109/img/letras.svg" height="25vh" style="padding-left:0.5rem;"></a>
                    </div>
                    <div class="top-right links">
                        <a style="color:#0D5DA7; font-family: 'Nunito'" href="{{ url('/home') }}"><h3><bold>QUIENES SOMOS LEARNING CONNECTION?</bold></h3></a>
                    </div>
                </div>
            @endif
            <div class="abs-center">
                <div class="row justfy-content-center" style="padding: 3%">
                    <div class="col-md-4">
                        <div class="row justfy-content-center" style="text-align: center;">:
                            <div class="col-4">
                                <img src="https://192.168.1.109/img/Isotipo.svg" height="110vh">
                                <br>
                                <br>
                                <img src="https://192.168.1.109/img/Tipography.svg" height="60vh">
                            </div>
                        </div>
                        <h2 style="margin-bottom:0.2rem" class="title" >BIENVENIDOS!</h2>
                        <span style="color:#0D5DA7; font-size:0.7vw;">A la plataforma virtual de educación</span>
                        <br>
                        <span style="color:#0D5DA7; font-size:0.7vw;">primaria Learning Connection.</span>
                        <div class="row justfy-content-md-center" style="text-align: center; padding: 2%">:
                            <div class="col-3">
                                <input class="btn blue" style="color: white; font-family: 'Nunito'; font-size:0.8vw; font-weight: bold; border-radius: 7px; height: 4vh; width: 7vw; margin-bottom:3%" type="button" onclick="location='login'" value="INICIAR SESIÓN">
                                <br>
                                <input class="btn blue"style="color: white; font-family: 'Nunito'; font-size:0.8vw; font-weight: bold; border-radius: 7px; height: 4vh; width: 7vw" type="button" onclick="location='register'" value="REGISTRARSE">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bkgn" style="height:0%">
            <img style="position:relative; overflow: hidden; bottom: 22vh; left:14vw" src="https://192.168.1.109/img/Ellipse5.svg" height="130vh">
            <img style="position:relative; overflow: hidden; bottom: 37vh; left:16vw" src="https://192.168.1.109/img/Ellipse5.svg" height="80vh">
            <img style="position:relative; overflow: hidden; bottom: 15vh; left:81.2vw" src="https://192.168.1.109/img/Ellipse4.svg" height="320vh">
        </div>
    </body>
</html>
