<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', function () {
    event(new \App\Events\chatReciver("hola", "@fmurillom", "4:51pm", "study", 2));
    return 'test';
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/admin', 'AdminController@index')->name('admin')->middleware('admin');

Route::get('/teacherp', 'TeacherProfileController@index')->name('teacherp')->middleware('teacher');

Route::get('/student', 'StudentController@index')->name('student')->middleware('student');

Route::get('/materialUser/{grade}/{subject}/{content}/{type}', 'MaterialUserController@index')->name('materialUser')->middleware('auth');

Route::get('/grade', 'GradeController@index')->name('grade')->middleware('admin');

Route::get('/subject', 'SubjectController@index')->name('subject')->middleware('admin');

Route::get('/content', 'ContentController@index')->name('content')->middleware('admin');

Route::get('/material', 'MaterialController@index')->name('material')->middleware('admin');

Route::get('/teacher', 'TeacherController@index')->name('teacher')->middleware('admin');

Route::get('/studyRoom', 'StudyRoomController@index')->name('studyRoom')->middleware('student');

Route::get('/questionRoom', 'QuestionRoomController@index')->name('questionRoom')->middleware('auth');

Route::get('/sroom/{id}', 'StudyRoomController@indexRoom')->name('sroom')->middleware('student');

Route::get('/profile', 'ProfileController@index')->name('profile')->middleware('auth');

Route::get('/teacherrequests', 'RequestsController@index')->name('teacherrequests')->middleware('teacher');

Route::get('/qroom/{id}', 'QuestionRoomController@indexRoom')->name('qroom')->middleware('auth');

Route::get('/acceptqroom/{id}', 'QuestionRoomController@acceptqroom')->name('acceptqroom')->middleware('auth');

Route::get('/tostudydelete/{study}', 'StudentController@deleteTask')->name('tostudydelete')->middleware('student');

Route::post('/student', 'StudentController@create')->name('createStudent')->middleware('auth');

Route::post('/profile', 'ProfileController@edit')->name('editUser')->middleware('auth');

Route::post('/materialU', 'MaterialUserController@update')->name('materialU')->middleware('auth');

Route::post('/grade', 'GradeController@create')->name('createGrade')->middleware('admin');

Route::post('/subject', 'SubjectController@create')->name('createSubject')->middleware('admin');

Route::post('/content', 'ContentController@create')->name('createContent')->middleware('admin');

Route::post('/material', 'MaterialController@create')->name('createMaterial')->middleware('admin');

Route::post('/teacher', 'TeacherController@create')->name('createTeacher')->middleware('admin');

Route::post('/teacherp', 'TeacherProfileController@create')->name('applyTeacher')->middleware('auth');

Route::post('/studyRoom', 'StudyRoomController@create')->name('createStudyRoom')->middleware('student');

Route::post('/sroom', 'StudyRoomController@addMaterial')->name('sroomMaterial')->middleware('student');

Route::post('/qroom', 'QuestionRoomController@addMaterial')->name('qroomMaterial')->middleware('auth');

Route::post('/teacher/{id}', 'TeacherController@accept')->name('acceptTeacher')->middleware('admin');

Route::post('/teacherrequests', 'RequestsController@create')->name('createrequests')->middleware('teacher');

Route::post('/tostudy', 'StudentController@createTask')->name('tostudy')->middleware('student');

Route::post('/material/{id}', 'MaterialController@accept')->name('acceptMaterial')->middleware('admin');

Route::post('/questionRoom', 'QuestionRoomController@create')->name('cquestionRoom')->middleware('auth');

Route::put('/studyRoom/{id}', 'StudyRoomController@exit')->name('exitStudyRoom')->middleware('student');

Route::delete('/grade/{id}', 'GradeController@destroy')->name('deleteGrade')->middleware('admin');

Route::delete('/subject/{id}', 'SubjectController@destroy')->name('deleteSubject')->middleware('admin');

Route::delete('/content/{id}', 'ContentController@destroy')->name('deleteContent')->middleware('admin');

Route::delete('/material/{id}', 'MaterialController@destroy')->name('deleteMaterial')->middleware('admin');

Route::delete('/teacher/{id}', 'TeacherController@destroy')->name('deleteTeacher')->middleware('admin');

Route::delete('/studyRoom/{id}', 'StudyRoomController@destroy')->name('deleteStudyRoom')->middleware('student');

Route::delete('/questionRoom/{id}', 'QuestionRoomController@destroy')->name('deleteQRoom')->middleware('auth');

Route::delete('/materialRoom/{id}', 'StudyRoomController@destroyM')->name('deleteRoomMaterial')->middleware('student'); 

Route::delete('/materialqRoom/{id}', 'QuestionRoomController@destroyM')->name('deleteqRoomMaterial')->middleware('auth'); 

Route::delete('/deleteProfile/{id}', 'ProfileController@destroy')->name('deleteProfile')->middleware('auth');